﻿function split(val) {
	return val.split(/,\s*/);
}
function extractLast(term) {
	return split(term).pop();
}
Date.shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
function short_months(dt) {
	return Date.shortMonths[dt.getMonth()];
}
function formatDate(date, format, seprator) {

	var d = new Date(date);
	if (seprator === "") {
		seprator = "-";
	}
	var output = "";
	month = '' + Date.shortMonths[d.getMonth()],
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;
	if (format === "ddMMyyy") {
		output = [day, month, year].join(seprator);
	}
	else {
		output = [month, day, year].join('-');
	}
	return output;
}

function OpenDialogWindow(id, title, type, content, height, width, button) {
	var dialogId = "#" + id;
	if (type == "N") {
		$(dialogId).html(content);
	}

	$(dialogId).dialog({
		resizable: false,
		title: title,
		height: height,
		width: width,
		modal: true,

		buttons: button
	});
}
function OpenContactDialog(Id, ParentId, dialogId) {
	var formData = $("#frmManageContact").serialize();
	//var form = new FormData($("#frmManageContact"));
	button = {
		"Yes": function () {

			$(this).dialog("close");
		},
		Cancel: function () {
			$(this).dialog("close");
		}
	}
	$("#" + dialogId).load("/Member/Contact/ManageCallContact/?id=" + Id + "&ParentId=" + ParentId, function () {
		OpenDialogWindow(dialogId, "Additional Contact", "U", "", 450, 700, {});
	});
}
function SaveContactDetails() {
	var formData = $("#frmManageContact").serialize();
	$.ajax({
		url: "/Member/Contact/ManageContact",
		dataType: "json",
		method: "POST",
		data: formData,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',

	}).done(function (response) {

		if (response.IsSuccess === true) {
			//MakeToast("1", response.Message);
			var Id = 0;
			var ContactId = $("#hdnLastCallContactId").val();
			$("#contactList").load("/Member/Contact/ContactDetailsPartial/?id=" + Id + "&ParentId=" + ContactId);
			$("#jsGrid1").jsGrid();
			$("#dlgContact").dialog("close");
		}
		else if (response.IsSuccess == false && response.IsErrorOccur == true) {
			MakeToast("3", response.Message);
			//$("#dlgContact").dialog("close");
		}
		else {
			MakeToast("2", response.Message);
			//$("#dlgContact").dialog("close");
		}

	});
}
function OpenPackageConfigurationDialog(Id, PackageGroupId, dialogId) {
	//var formData = $("#frmManageContact").serialize();
	//var form = new FormData($("#frmManageContact"));
	button = {
		"Yes": function () {

			$(this).dialog("close");
		},
		Cancel: function () {
			$(this).dialog("close");
		}
	}
	$("#" + dialogId).load("/Admin/PackageGroup/ManageConfigurationPartial/?id=" + Id + "&PackageGroupId=" + PackageGroupId, function () {
		OpenDialogWindow(dialogId, "Configuration", "U", "", 200, 700, {});
	});
}

function SaveConfigurationDetails() {

	var formData = $("#frmManageConfig").serialize();
	$.ajax({
		url: "/Admin/PackageGroup/ManageConfiguration",
		dataType: "json",
		method: "POST",
		data: formData,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',

	}).done(function (response) {

		if (response.IsSuccess == true) {
			MakeToast("1", response.Message);
			var Id = 0;

			var PackageGroupId = $("#PackageGroupId").val();
			window.location.href = "/Admin/PackageGroup/GroupDetails/?id=" + PackageGroupId;
			$("#dlgPackageConfig").dialog("close");
		}
		else if (response.IsSuccess == false && response.IsErrorOccur == true) {
			MakeToast("3", response.Message);
			$("#dlgPackageConfig").dialog("close");
		}
		else {
			MakeToast("2", response.Message);
			$("#dlgPackageConfig").dialog("close");
		}


	});
}
function OpenCallHistoryDialog(Id, ContactId, CallId, dialogId) {
	button = {
		"Yes": function () {
			$(this).dialog("close");
		},
		Cancel: function () {
			$(this).dialog("close");
		}
	}
	$("#" + dialogId).load("/Member/Calls/CallHistory/?id=" + Id + "&ContactId=" + ContactId + "&CallId=" + CallId, function () {
		OpenDialogWindow(dialogId, "Call History", "U", "", 400, 700, {});
	});
}
function SaveProductDetails() {
	var formData = $("#frmManageProduct").serialize();
	$.ajax({
		url: "/Member/Product/ManageContactProduct",
		dataType: "json",
		method: "POST",
		data: formData,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',

	}).done(function (response) {

		if (response.IsSuccess == true) {
			//MakeToast("1", response.Message);
			var Id = 0;
			var ContactId = $("#hdnLastCallContactId").val();
			var CallId = $("#hdnLastCallCallId").val();
			$("#poductDetails").load("/Member/Product/ContactProductDetails/?id=" + Id + "&ContactId=" + ContactId);
			$("#dlgProduct").dialog("close");
		}
		else if (response.IsSuccess === false && response.IsErrorOccur === true) {
			MakeToast("3", response.Message);
			//$("#dlgProduct").dialog("close");
		}
		else {
			MakeToast("2", response.Message);
			//$("#dlgProduct").dialog("close");
		}


	});
}
function OpenProductDialog(Id, ContactId, CallId, dialogId) {

	button = {
		"Yes": function () {

			$(this).dialog("close");
		},
		Cancel: function () {
			$(this).dialog("close");
		}
	}
	$("#" + dialogId).load("/Member/Product/ContactProduct/?id=" + Id + "&ContactId=" + ContactId + "&CallId=" + CallId, function () {
		OpenDialogWindow(dialogId, "Add Product", "U", "", 200, 700, {});
	});
}
function OpenContactImportDialog(dialogId) {


	$("#" + dialogId).load("/Member/Contact/ImportContactPartial", function () {
		OpenDialogWindow(dialogId, "Import Contact", "U", "", 150, 400, {});
	});
}
function MakeToast(Type, Message) {
	if (Type == "1") {
		toastr.success(Message);
	}
	else if (Type == "2") {
		toastr.warning(Message);

	}
	else if (Type == "3") {
		toastr.error(Message);
	}
}
$(document).ready(function () {
	/*	$("#OrganisationName")
			.bind("keydown", function (event) {
	
				if (event.keyCode == 188) {
					$(this).val('');
					event.preventDefault();
				}
				if (event.keyCode === $.ui.keyCode.TAB &&
					$(this).data("ui-autocomplete").menu.active) {
					event.preventDefault();
				}
	
			})
			.autocomplete({
				source: function (request, response) {
					$.ajax({
						url: '/Member/Organization/FetchOrganization',
						dataType: "json",
						method:"GET",
						data: {
							SearchText: extractLast(request.term)
						},
						success: function (data) {
							console.log(data);
							response(data);
						}
					});
				},
				minLength: 2,
				change: function (event, ui) {
	
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.text);
					$("#OrganisationId").val(ui.item.value);
					if (ui.item.value != 0) {
						if (ui.item.text.indexOf("No Match Found") < 0) {
	
							$(this).val(ui.item.text);
							$("#OrganisationId").val(ui.item.value);
						}
					}
					else {
						//this.value = '';
						$("#OrganisationId").val("0");
					}
				},
				focus: function () {
					// prevent value inserted on focus
					return false;
				}
			}).autocomplete("instance")._renderItem = function (ul, item) {
				return $("<li>")
					.append("<div>" + item.label + "</div>")
					.appendTo(ul);
			};*/
});
