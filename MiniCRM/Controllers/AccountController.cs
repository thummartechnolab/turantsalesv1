﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MiniCRM.Models;
using CRMModel;
using CRMModel.ViewModel;
using CRMBAL;
using CRMUtility;

namespace MiniCRM.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            _roleManager = roleManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set { _roleManager = value; }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var CurrentUser = await User_BAL.GetUserByUserName(model.Email);
            if (CurrentUser != null)
            {
                bool IsApproved = false;
                if (CurrentUser.UserTypeCode == "AA" || CurrentUser.UserTypeCode == "SA")
                {
                    IsApproved = true;
                }
                else
                {
                    IsApproved = CurrentUser.IsApproved;
                }
                if (!CurrentUser.IsDeleted && CurrentUser.IsActive)
                {
                    if (IsApproved)
                    {
                        var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
                        switch (result)
                        {
                            case SignInStatus.Success:
                                string UserTypeCode = "";

                                if (CurrentUser != null)
                                {
                                    var rsp = Response;
                                    UserCookie cookie = new UserCookie();
                                    cookie.UserId = CurrentUser.UserId;
                                    cookie.UserTypeId = CurrentUser.UserTypeId;
                                    cookie.UserTypeCode = CurrentUser.UserTypeCode;
                                    cookie.UserName = CurrentUser.FirstName;
                                    bool IsCookieSet = CookieHandler.SetCookie(ref rsp, cookie);
                                    if (IsCookieSet)
                                    {
                                        bool IsSessionSet = ResetUserSession.ResetSession(cookie);
                                    }
                                    UserTypeCode = cookie.UserTypeCode;

                                   await User_BAL.UpdateLastLogin(CurrentUser.UserId);

                                }

                                return RedirectToLocal(returnUrl, UserTypeCode);
                            case SignInStatus.LockedOut:
                                return View("Lockout");
                            case SignInStatus.RequiresVerification:
                                return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                            case SignInStatus.Failure:
                            default:
                                ModelState.AddModelError("", "Invalid login attempt.");
                                return View(model);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Your account was not approved.Please contact to our support team.");

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Your account has been deactivated.");

                }

            }
            else
            {
                ModelState.AddModelError("", "Invalid login attempt.");

            }
            return View(model);
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        #region ---Fill DropDown List---
        public void FillDropDownList(ref RegisterViewModel model)
        {
            var PackageList = CRMBAL.Package_BAL.GetPackages();
            model.Packages = PackageList.Where(P => P.PackageName.Contains("Free")).ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            items = Common_BAL.GenderDropdown();
            model.GenderList = items;

        }
        #endregion
        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes.
            // If a user enters incorrect codes for a specified amount of time then the user account
            // will be locked out for a specified amount of time.
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            RegisterViewModel model = new RegisterViewModel();
            FillDropDownList(ref model);
            return View(model);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    IdentityResult roleresult = null;
                    roleresult = await UserManager.AddToRoleAsync(user.Id, En_Roles.Member.ToString());
                    if (roleresult.Succeeded)
                    {
                        User_mdl mdl = new User_mdl();
                        mdl.UserUniqueId = user.Id;
                        mdl.FirstName = model.FirstName;
                        mdl.LastName = model.LastName;
                        mdl.Email = model.Email;
                        mdl.ContactNo = model.ContactNo;
                        mdl.Gender = model.Gender;
                        mdl.PackageId = model.PackageId;
                        mdl.UserTypeId = 3;
                        var response = await User_BAL.Add(mdl);
                        if (response.IsSuccess)
                        {
                            var PackageDetails = Package_BAL.GetPackageById(model.PackageId);
                            if (PackageDetails != null)
                            {
                                if (PackageDetails.PackageName.Contains("Free"))
                                {
                                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                                }
                            }
                            var rsp = Response;
                            UserCookie cookie = new UserCookie();
                            cookie.UserId = response.Id;
                            cookie.UserTypeId = 3;
                            cookie.UserTypeCode = "MR";
                            cookie.UserName = model.FirstName;
                            bool IsCookieSet = CookieHandler.SetCookie(ref rsp, cookie);
                            if (IsCookieSet)
                            {
                                bool IsSessionSet = ResetUserSession.ResetSession(cookie);
                            }
                            // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                            // Send an email with this link
                            // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                            var callbackUrl = Url.Action("Terms", "Home", new { }, protocol: Request.Url.Scheme);
                            var login = Url.Action("Login", "Account", new { }, protocol: Request.Url.Scheme);
                            // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                            string msgbody = @"<html><body>Dear Customer,<br/>
Your new Turant Sales Account has been created.<br/>Welcome to the Turant Sales community.<br>
 <a href='" + callbackUrl + "'>Please read T&C .......</a> <br/> <a href='" + login + "'>Login Now!.....</a> <br/>We are here to help , please feel free to contact us at info@turantsales.com</body></html>";
                            Utility.SendMail(msgbody, model.Email, "Welcome to Turant Sales");
                            return RedirectToLocal("", "MR");
                            //return RedirectToAction("Index", "Home");
                        }
                    }
                }
                FillDropDownList(ref model);
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                string body = "We have got a request to change your Turant Sales password ! <br/>" +
                    "<a href=\"" + callbackUrl + "\">RESET PASSWORD</a>";
                Utility.SendMail(body, model.Email, "Reset your Turant Sales Password");
                // body = "We have got a request to change your Turant Sales password !" + callbackUrl;
                // await UserManager.SendEmailAsync(user.Id, "Reset your Turant Sales Password", body);
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }
        public string CheckSessionAliveV1()
        {
            string res = "";
            var cookie = Response;
            int durationInSeconds = ((System.Web.HttpContext.Current.Session.Timeout * 60));
            int durationInMicroSeconds = (durationInSeconds - (3 * 60)) * 1000;
            string url = HttpContext.Request.Url.AbsoluteUri;

            if (Session[En_Session.StartTime.ToString()] == null)
            {
                //if URL is either Login / or First page like /Flight /Hotel etc
                if (url.IndexOf("/Login") > -1)
                {
                    //Then it should go ahead
                }
                else
                {
                    if (Session["UserId"] == null || Session["UserId"] != null)//UserSession is Live then
                    {
                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        CookieHandler.DeleteCookie(ref cookie);
                        res = Url.Action("Index", "Home", new { Areas = "" });
                    }

                }
            }
            else if ((DateTime.Now - Convert.ToDateTime(Session[En_Session.StartTime.ToString()])).Minutes > System.Web.HttpContext.Current.Session.Timeout)
            {//CurrentTime - TTS_StartTime > 18 Minutes ( Time out )


                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                CookieHandler.DeleteCookie(ref cookie);
                res = Url.Action("Index", "Home", new { Areas = "" }); //Url.Action("Login", "Account");
                //Redirect to Home page
            }

            return res;

        }
        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl, string UserTypeCode = "")
        {

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            if (!String.IsNullOrEmpty(UserTypeCode))
            {
                if (UserTypeCode == En_UserTypeCode.MR.ToString())
                {
                    return RedirectToAction("Index", "Calls", new { area = "Member" });
                }
                else if (UserTypeCode == En_UserTypeCode.AA.ToString() || UserTypeCode == En_UserTypeCode.SA.ToString())
                {
                    return RedirectToAction("Index", "Home", new { area = "Admin" });
                }
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}