﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using CRMUtility;
using CRMModel;
using MiniCRM.Models;
using System.Web.Configuration;

namespace MiniCRM.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                var _request = Request;
                bool IsUser = CookieHandler.GetCookie(ref _request);
                if (IsUser)
                {
                    string UserTypeCode = UserSession.UserTypeCode;
                    if (UserTypeCode == En_UserTypeCode.MR.ToString())
                    {
                        return RedirectToAction("Index", "Calls", new { area = "Member" });
                    }
                    else if (UserTypeCode == En_UserTypeCode.AA.ToString() || UserTypeCode == En_UserTypeCode.SA.ToString())
                    {
                        return RedirectToAction("Index", "Home", new { area = "Admin" });
                    }
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "About Us";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Us";

            return View();
        }
        [HttpPost]
        public ActionResult Contact(ContactViewModel mdl)
        {
            if (ModelState.IsValid)
            {
                string Email = WebConfigurationManager.AppSettings["ContactMail"];
                string Message = "Name :" + mdl.Name + "<br/> Email :" + mdl.Email + "<br/> Phone :" + mdl.Mobile + "<br/> Comment :" + mdl.Comment;
                Utility.SendMail(Message, Email, "Contact :" + mdl.Name);
                Utility.SendMail("Thank You for Contact Us We have Contact Sortly", mdl.Email, "Turant Sales");
                TempData["Successs"] = "Send Contact Request Successfully";

            }
            return RedirectToAction("Contact");
        }
        public ActionResult Howtouse()
        {
            ViewBag.Message = "How to use";

            return View();
        }
        public ActionResult Faq()
        {
            ViewBag.Message = "FAQ";

            return View();
        }

        public ActionResult Privacy()
        {
            ViewBag.Message = "Privacy Policy";
            return View();
        }
        public ActionResult Terms()
        {
            ViewBag.Message = "Terms & Condition";
            return View();
        }
        public ActionResult Payment() {
            ViewBag.Message = "How To Payment";
            return View();
        }
    }
}