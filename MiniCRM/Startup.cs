﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MiniCRM.Startup))]
namespace MiniCRM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
