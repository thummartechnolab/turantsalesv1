﻿using System.Web;
using System.Web.Optimization;

namespace MiniCRM
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                     "~/Content/Main/js/jquery-ui-1.10.3.custom.min.js",
                     "~/Content/Main/js/jquery.ui.touch-punch.min.js",
                     "~/Content/Main/js/bootstrap.min.js",
                     "~/Content/Main/js/bootstrap-select.js",
                     "~/Content/Main/js/bootstrap-switch.js",
                     "~/Content/Main/js/flatui-checkbox.js",
                     "~/Content/Main/js/flatui-radio.js",
                     "~/Content/Main/js/jquery.tagsinput.js",
                     "~/Content/Main/js/jquery.placeholder.js",
                     "~/Content/Main/js/bootstrap-typeahead.js",
                     "~/Content/Main/js/application.js",
                     "~/Content/Main/js/modernizr.custom.js",
                     "~/Content/Main/js/toucheffects.js",
                     "~/Content/Main/js/jquery.mixitup.js",
                     "~/Content/Main/rs-plugin/js/jquery.themepunch.plugins.min.js",
                     "~/Content/Main/rs-plugin/js/jquery.themepunch.revolution.min.js",
                     "~/Content/Main/showbizpro/js/jquery.themepunch.showbizpro.min.js",
                     "~/Content/Main/js/jquery.scrollUp.js",
                     "~/Content/Main/js/theme.js"
                     ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Main/bootstrap/css/bootstrap.css",
                      "~/Content/Main/css/ui.css",
                      "~/Content/Main/css/base.css",
                      "~/Content/Main/css/style-blue.css",
                      "~/Content/Main/css/font-awesome.min.css",
                      "~/Content/Main/rs-plugin/css/settings.css",
                      "~/Content/Main/showbizpro/css/settings.css"
                      ));
        }
    }
}
