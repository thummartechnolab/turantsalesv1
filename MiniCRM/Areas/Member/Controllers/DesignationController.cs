﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRMBAL;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;

namespace MiniCRM.Areas.Member.Controllers
{
    public class DesignationController : Controller
    {
        // GET: Member/Designation
        #region --Json Method--
        public JsonResult GetDesignationList()
        {
            var data = HttpContext.Request.QueryString;
            var list = Designation_BAL.GetDesignationJsGridList(data, UserSession.UserId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchDesignation(string SearchText)
        {
            var response = Designation_BAL.GetDesignationAutoComplete(UserSession.UserId, SearchText);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ManageDesignation(SP_Select_Designation mdl)
        {
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(mdl.Designation))
            {
                bool IsAleradyExists = Designation_BAL.IsDesignationAleradyExists(mdl.Designation, mdl.Id,UserSession.UserId);
                if (mdl.Id > 0)
                {
                    if (!IsAleradyExists)
                    {
                        response = Designation_BAL.Update(mdl).Result;

                    }
                }
                else
                {
                    if (!IsAleradyExists)
                    {
                        response = Designation_BAL.Add(mdl).Result;
                    }
                }
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Industry Type Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Designation_BAL.IsdesignationExists(id);
            if (IsExists)
            {
                var Op = Designation_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
    }
}