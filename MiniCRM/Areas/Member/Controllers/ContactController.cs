﻿using CRMBAL;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;
using CRMEntity;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Hosting;

namespace MiniCRM.Areas.Member.Controllers
{
    [Authorize(Roles = "Member")]
    public class ContactController : Controller
    {
        #region ---Json Method---
        [HttpGet]
        public JsonResult FetchContact(string SearchText)
        {
            var response = Contact_BAL.GetConatctAutoComplete(UserSession.UserId, SearchText);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchLastName(string SearchText)
        {
            var response = Contact_BAL.GetLastNameAutoComplete(UserSession.UserId, SearchText);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchCity(string SearchText)
        {
            var response = Contact_BAL.GetCityAutoComplete(UserSession.UserId, SearchText);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetCustomerType()
        {
            var response = Contact_BAL.GetCustomerTypes();
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetContactList()
        {
            var data = HttpContext.Request.QueryString;

            var list = Contact_BAL.GetContactJsGridList(data, UserSession.UserId, 0);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetContactDetails(long id)
        {
            List<SP_Select_Contact> select_Contacts = new List<SP_Select_Contact>();
            select_Contacts = Contact_BAL.GetContacts(id, UserSession.UserId, id);
            var data = select_Contacts.FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        #endregion
        // GET: Member/Contact
        public ActionResult Index()
        {

            string dir = HostingEnvironment.MapPath("~/Uploads");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return View();
        }
        public void FillDropDown()
        {
            ViewBag.CustomerTypeList = Contact_BAL.GetCustomerTypes();
        }
        public ActionResult ManageContact(long id = 0, long ParentId = 0)
        {
            SP_Select_Contact _Contact = new SP_Select_Contact();
            try
            {


                if (id > 0)
                {

                    var response = Contact_BAL.GetContacts(ParentId, UserSession.UserId, id);
                    if (response != null)
                    {
                        if (response.Count() > 0)
                        {
                            _Contact = response.FirstOrDefault();
                        }
                    }
                }
                else
                {
                    _Contact.ParentContactId = ParentId;
                }
            }
            catch (Exception ex)
            {

            }
            FillDropDown();
            return View(_Contact);
        }
        [HttpPost]
        public JsonResult ManageContact(SP_Select_Contact mdl)
        {
            Response_mdl response = new Response_mdl();
            if (mdl.Id > 0)
            {
                response = Contact_BAL.VerifyMaster("Modify", mdl).Result;

            }
            else
            {
                int ContactCount = 0; long ParentContactId = 0;
                if (mdl.ParentContactId == null || mdl.ParentContactId == 0)
                {
                    ParentContactId = mdl.Id;
                }
                else
                {
                    ParentContactId = mdl.ParentContactId;
                }
                var select_Contacts = Contact_BAL.GetContacts(ParentContactId, UserSession.UserId, 0);
                if (select_Contacts != null)
                {
                    ContactCount = select_Contacts.Count();
                }
                if (ContactCount < 3)
                {
                    response = Contact_BAL.VerifyMaster("Add", mdl).Result;
                }
                else
                {
                    response.IsSuccess = false; response.IsErrorOccur = true;
                    response.Message = "You Can't add Contact";

                }

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageCallContact(long id = 0, long ParentId = 0)
        {
            SP_Select_Contact _Contact = new SP_Select_Contact();
            try
            {
                if (ParentId == 0) { ParentId = id; }
                var response = Contact_BAL.GetContacts(ParentId, UserSession.UserId, id);
                if (id > 0)
                {

                    if (response != null)
                    {
                        if (response.Count() > 0)
                        {
                            _Contact = response.FirstOrDefault();
                        }
                    }
                }
                else
                {
                    var cnt = response.FirstOrDefault();
                    if (cnt != null)
                    {
                        _Contact.CityId = cnt.CityId;
                        _Contact.CityName = cnt.CityName;
                        _Contact.OrganisationId = cnt.OrganisationId;
                        _Contact.OrganisationName = cnt.OrganisationName;
                        _Contact.IndustryType = cnt.IndustryType;
                        _Contact.IndustryTypeId = cnt.IndustryTypeId;

                    }
                    _Contact.ParentContactId = ParentId;
                }
            }
            catch (Exception ex)
            {

            }
            FillDropDown();
            return PartialView("_CreateOrUpdate", _Contact);
        }
        public ActionResult ContactDetails(long id = 0, long ParentId = 0)
        {
            List<SP_Select_Contact> select_Contacts = new List<SP_Select_Contact>();
            try
            {
                select_Contacts = Contact_BAL.GetContacts(ParentId, UserSession.UserId, id);
            }
            catch (Exception ex)
            {

                throw;
            }
            return View(select_Contacts);
        }
        public ActionResult ContactDetailsPartial(long id = 0, long ParentId = 0)
        {
            List<SP_Select_Contact> select_Contacts = new List<SP_Select_Contact>();
            try
            {
                select_Contacts = Contact_BAL.GetContacts(ParentId, UserSession.UserId, id);
            }
            catch (Exception ex)
            {

                throw;
            }
            return PartialView("_ContactDetails", select_Contacts);
        }
        public ActionResult ImportContactPartial()
        {
            return PartialView("_ContactImport");
        }

        public string ImportContactDetail(HttpPostedFileBase ResponseDocument)
        {
            try
            {
                string Status = string.Empty;

                if (Request.Files.Count > 0)
                {
                    if (Request.Files[0].ContentLength > 0)
                    {
                        HttpPostedFileBase postedFile = Request.Files[0];
                        string filename = "Doc_" + Guid.NewGuid().ToString();
                        string strLocation = HttpContext.Server.MapPath("~/Uploads");
                        Request.Files[0].SaveAs(strLocation + @"\" + filename + Path.GetExtension(postedFile.FileName));
                        DataTable CsvData = GetDataTabletFromCSVFile(strLocation + @"\" + filename + Path.GetExtension(postedFile.FileName));
                        Session["Path"] = strLocation + @"\" + filename + Path.GetExtension(postedFile.FileName);
                        if (CsvData != null && CsvData.Rows.Count > 0)
                        {
                            CsvData.Rows.RemoveAt(0);
                            Status = FillContactModel(CsvData);
                            if (Status == "Success")
                            {
                                ViewBag.successmsg = "Contact have been successfully saved.";
                                return "Success";
                            }
                        }
                        else
                        {
                            //return "Problem occurred while insert Supplier wise Location,Try again.";
                            ViewBag.errormsg = "Problem occurred while change Contact ,Try again.";
                            return "Failed";
                        }
                    }
                    else
                    {
                        ViewBag.errormsg = "File is Empty.";
                        return "Failed";
                        //return "File is Empty.";
                    }

                }
                else
                {
                    ViewBag.errormsg = "Please Upload a CSV File.";
                    //return "Please Upload a CSV File.";
                    return "InCorrectFile";
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public ActionResult Download()
        {
            string Path = Server.MapPath("~/Content/Sample/Sample.csv");
            return File(Path,"text/csv", "Sample.csv");
        }

        public static DataTable GetDataTabletFromCSVFile(string FilePath)
        {
            DataTable csvData = new DataTable();
            int k = 0;
            try
            {

                //var csvTable = new DataTable();
                //using (var csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(@"D:\CSVFolder\CSVFile.csv")), true))
                //{
                //    csvTable.Load(csvReader);
                //}n

                using (System.IO.StreamReader csvReader = new System.IO.StreamReader(FilePath))
                {
                    csvData.Columns.Add("OragnisationName");
                    csvData.Columns.Add("FirstName");
                    csvData.Columns.Add("LastName");
                    csvData.Columns.Add("Phone 1");
                    csvData.Columns.Add("Phone 2");
                    csvData.Columns.Add("Extn");
                    csvData.Columns.Add("Email 1");
                    csvData.Columns.Add("Email 2");
                    csvData.Columns.Add("Chat 1");
                    csvData.Columns.Add("Chat 2");
                    csvData.Columns.Add("Designation");
                    //csvData.Columns.Add("Product 1");
                    //csvData.Columns.Add("Product 2");
                    //csvData.Columns.Add("Product 3");
                    csvData.Columns.Add("Address1");
                    csvData.Columns.Add("Street");
                    //csvData.Columns.Add("Area");
                    csvData.Columns.Add("City");
                    //csvData.Columns.Add("State");
                    // csvData.Columns.Add("Country");
                    csvData.Columns.Add("Pincode");
                    csvData.Columns.Add("UserId");
                    string s = "";
                    Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");


                    while (!csvReader.EndOfStream)
                    {
                        csvData.Rows.Add();

                        s = csvReader.ReadLine();



                        String[] Fields = CSVParser.Split(s);

                        //string[] fieldData = csvReader.ReadLine().Split(',');

                        for (int i = 0; i < Fields.Length; i++)
                        {
                            Fields[i] = Fields[i].TrimStart(' ', '"');
                            Fields[i] = Fields[i].TrimEnd('"');
                        }

                        if (k == 0)
                        {
                            csvData.Rows[csvData.Rows.Count - 1][0] = Fields[0];
                            csvData.Rows[csvData.Rows.Count - 1][1] = Fields[1];
                            csvData.Rows[csvData.Rows.Count - 1][2] = Fields[2];
                            csvData.Rows[csvData.Rows.Count - 1][3] = Fields[3];
                            csvData.Rows[csvData.Rows.Count - 1][4] = Fields[4];
                            csvData.Rows[csvData.Rows.Count - 1][5] = Fields[5];
                            csvData.Rows[csvData.Rows.Count - 1][6] = Fields[6];
                            csvData.Rows[csvData.Rows.Count - 1][7] = Fields[7];
                            csvData.Rows[csvData.Rows.Count - 1][8] = Fields[8];
                            csvData.Rows[csvData.Rows.Count - 1][9] = Fields[9];
                            csvData.Rows[csvData.Rows.Count - 1][10] = Fields[10];
                            csvData.Rows[csvData.Rows.Count - 1][11] = Fields[11];
                            csvData.Rows[csvData.Rows.Count - 1][12] = Fields[12];
                            csvData.Rows[csvData.Rows.Count - 1][13] = Fields[13];
                            csvData.Rows[csvData.Rows.Count - 1][14] = Fields[14];
                            csvData.Rows[csvData.Rows.Count - 1][15] = "UserId";
                        }
                        else
                        {
                            //bool Is_numeric = STPUtilities.STPFunctions.checke_IsnumericorString(Fields[6]);
                            csvData.Rows[csvData.Rows.Count - 1][0] = Fields[0];// Convert.ToInt32(Fields[0]);
                            csvData.Rows[csvData.Rows.Count - 1][1] = Fields[1];
                            csvData.Rows[csvData.Rows.Count - 1][2] = Fields[2];
                            csvData.Rows[csvData.Rows.Count - 1][3] = Fields[3];
                            csvData.Rows[csvData.Rows.Count - 1][4] = Fields[4];
                            csvData.Rows[csvData.Rows.Count - 1][5] = Fields[5];
                            csvData.Rows[csvData.Rows.Count - 1][6] = Fields[6];// Convert.ToInt32(Fields[0]);
                            csvData.Rows[csvData.Rows.Count - 1][7] = Fields[7];
                            csvData.Rows[csvData.Rows.Count - 1][8] = Fields[8];
                            csvData.Rows[csvData.Rows.Count - 1][9] = Fields[9];
                            csvData.Rows[csvData.Rows.Count - 1][10] = Fields[10];
                            csvData.Rows[csvData.Rows.Count - 1][11] = Fields[11];
                            csvData.Rows[csvData.Rows.Count - 1][12] = Fields[12];
                            csvData.Rows[csvData.Rows.Count - 1][13] = Fields[13];
                            csvData.Rows[csvData.Rows.Count - 1][14] = Fields[14];
                            csvData.Rows[csvData.Rows.Count - 1][15] = UserSession.UserId;
                            //csvData.Rows[csvData.Rows.Count - 1][6] = string.IsNullOrEmpty(Fields[6]) ? false : Is_numeric ? Convert.ToBoolean(Convert.ToInt32(Fields[6])) : Convert.ToBoolean(Fields[6]);//Fields[6];
                        }
                        k++;

                    }
                }
                return csvData;
            }
            catch (Exception ex)
            {
                csvData = null;
                return csvData;

            }
        }

        public static string FillContactModel(DataTable csvdt)
        {

            try
            {
                string sqlconn;
                sqlconn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                using (SqlConnection con = new SqlConnection(sqlconn))
                {
                    using (SqlCommand cmd = new SqlCommand("spContactImport"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Lstcontact", csvdt);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return "Failed";
            }

        }

        public void ExportAllContact()
        {
            var AllData = Contact_BAL.GetContact_Master();
            #region
            StringBuilder sb = new StringBuilder();

            sb.Clear();

            sb.Append("OragnisationName,FirstName,LastName,Phone 1,Phone 2,Extn,Email 1,Email 2,Chat 1,Chat 2,Designation,Unit No,Street,City,Pincode");

            sb.Append("\r\n");
            //int Co = 0;
            foreach (var UserDetail in AllData)
            {
                if (UserDetail.OrganisationId != null)
                {
                    Organisation_Master OrganisationMaster = Organization_BAL.Fetch_Organisation_Master_by_ID(Convert.ToInt32(UserDetail.OrganisationId));
                    if (OrganisationMaster != null)
                    {
                        sb.Append(OrganisationMaster.OrganisationName + ",");
                    }
                    else
                    {
                        sb.Append("" + ",");
                    }
                }

                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.FirstName != null)
                { sb.Append(UserDetail.FirstName.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.LastName != null)
                { sb.Append(UserDetail.LastName.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.PrimaryPhoneNo != null)
                { sb.Append(UserDetail.PrimaryPhoneNo.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.PhoneNo3 != null)
                { sb.Append(UserDetail.PhoneNo2.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.Extn != null)
                { sb.Append(UserDetail.Extn.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.PrimaryEmail != null)
                { sb.Append(UserDetail.PrimaryEmail.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.ScondaryEmail != null)
                { sb.Append(UserDetail.ScondaryEmail.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.Chat1 != null)
                { sb.Append(UserDetail.Chat1.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.Chat2 != null)
                { sb.Append(UserDetail.Chat2.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }


                if (UserDetail.DesignationId != null)
                {
                    Designation_Master DesignationMaster = Designation_BAL.Fetch_Designation_Master_by_ID(Convert.ToInt32(UserDetail.DesignationId));
                    if (DesignationMaster != null)
                    {
                        sb.Append(DesignationMaster.Designation + ",");
                    }
                    else
                    {
                        sb.Append("" + ",");
                    }

                }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.UnitNo != null)
                { sb.Append(UserDetail.UnitNo.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.Street != null)
                { sb.Append(UserDetail.Street.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }


                if (UserDetail.CityId != null)
                {
                    City_Master city_Master = City_BAL.Fetch_City_Master_by_ID(Convert.ToInt32(UserDetail.CityId));
                    if (city_Master != null)
                    {
                        sb.Append(city_Master.CityName + ",");
                    }
                    else
                    {
                        sb.Append("" + ",");
                    }

                }
                else
                {
                    sb.Append("" + ",");
                }
                if (UserDetail.PinCode != null)
                { sb.Append(UserDetail.PinCode.ToString() + ","); }
                else
                {
                    sb.Append("" + ",");
                }

                //sb.Append(UserDetail.OrganisationId.ToString() + "," + '"' + UserDetail.FirstName.Replace("\r\n", "") + '"' + "," + '"' + UserDetail.LastName.Replace("\r\n", "") + '"' + "," + '"' + UserDetail.PhoneNo2 + '"' + ",");
                //sb.Append(UserDetail.PhoneNo3.ToString() + "," + '"' + UserDetail.Extn.Replace("\r\n", "") + '"' + "," + '"' + UserDetail.PrimaryEmail.Replace("\r\n", "") + '"' + "," + '"' + UserDetail.ScondaryEmail + '"' + ",");
                //sb.Append(UserDetail.Chat1.ToString() + "," + '"' + UserDetail.Chat2.Replace("\r\n", "") + '"' + "," + '"' + UserDetail.DesignationId + '"' + "," + '"' + UserDetail.UnitNo + '"' + ",");
                //sb.Append(UserDetail.Street.ToString() + "," + '"' + UserDetail.CityId + '"' + "," + '"' + UserDetail.PinCode + '"' + ",");


                sb.Append("\r\n");
                // Co++;
            }

            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/vnd.ms-excel";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            // Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=AllContact.csv");
            //Response.Charset = "";
            // Response.Charset = encoding.EncodingName;

            Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());

            Response.Write(sb.ToString());
            Response.Flush();
            Response.End();
            #endregion
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Contact_BAL.IsContactExists(id);
            if (IsExists)
            {
                var Op = Contact_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
    }
}