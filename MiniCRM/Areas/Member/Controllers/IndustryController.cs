﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRMBAL;
using CRMUtility;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
namespace MiniCRM.Areas.Member.Controllers
{
    [Authorize(Roles = "Member")]
    public class IndustryController : Controller
    {
        #region --Json Method--
        public JsonResult GetIndustryList()
        {
            var data = HttpContext.Request.QueryString;
            var list = Industry_BAL.GetIndustryJsGridList(data, UserSession.UserId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion
        // GET: Member/Industry
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ManageIndustry(SP_Select_Industry mdl)
        {
            Response_mdl response = new Response_mdl();

            bool IsAllowed = false;
            var UserRestriction = Common_BAL.VerifyUserAllowToPerformAction(En_RestrictionKey.IndustryType.ToString(), UserSession.UserId, ref IsAllowed);

            if (!String.IsNullOrEmpty(mdl.IndustryType))
            {
                bool IsAleradyExists = Industry_BAL.IsIndustryAleradyExists(mdl.IndustryType, mdl.Id, UserSession.UserId);
                if (mdl.Id > 0)
                {
                    if (!IsAleradyExists)
                    {
                        response = Industry_BAL.Update(mdl).Result;

                    }
                }
                else
                {
                    if (IsAllowed)
                    {
                        response = Industry_BAL.Add(mdl).Result;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = "Industry Type Required";
                    }
                }
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "You have reached your limit please contact to administrator.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Industry_BAL.IsIndustryExists(id);
            if (IsExists)
            {
                var Op = Industry_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
    }
}