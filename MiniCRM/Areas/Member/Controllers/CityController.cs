﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRMBAL;
using CRMUtility;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;

namespace MiniCRM.Areas.Member.Controllers
{
    public class CityController : Controller
    {
        #region --Json Method--
        public JsonResult GetCityList()
        {
            var data = HttpContext.Request.QueryString;
            var list = City_BAL.GetCityJsGridList(data, UserSession.UserId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion
        // GET: Member/City
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ManageCity(SP_Select_City mdl)
        {
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(mdl.CityName))
            {
                bool IsAleradyExists = City_BAL.IsCityAleradyExists(mdl.CityName, mdl.Id,UserSession.UserId);
                if (mdl.Id > 0)
                {
                    if (!IsAleradyExists)
                    {
                        response = City_BAL.Update(mdl).Result;

                    }
                }
                else
                {
                    if (!IsAleradyExists)
                    {
                        response = City_BAL.Add(mdl).Result;
                    }
                }
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "City Type Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = City_BAL.IsCityExists(id);
            if (IsExists)
            {
                var Op = City_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
    }
}