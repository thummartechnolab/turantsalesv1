﻿using CRMBAL;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniCRM.Areas.Member.Controllers
{
    [Authorize(Roles = "Member")]
    public class CallsController : Controller
    {
        #region --Json Method--
        public JsonResult GetCallList(string id)
        {
            var data = HttpContext.Request.QueryString;
            long IndustryId = 0;
            if (!String.IsNullOrEmpty(id) && id != "undefined")
            {
                IndustryId = Convert.ToInt64(id);
            }
            Session["IndustryId"] = IndustryId;
            var list = Call_BAL.GetCallJsGridList(data, UserSession.UserId, IndustryId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getLastCallDetails(string id)
        {
            var data = HttpContext.Request.QueryString;
            long IndustryId = 0;
            if (!String.IsNullOrEmpty(id) && id != "undefined")
            {
                IndustryId = Convert.ToInt64(id);
            }
            var list = Call_BAL.GetCallJsGridList(data, UserSession.UserId, IndustryId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion
        public void FillDropDown()
        {
            var IndustryList = Industry_BAL.GetIndustries(UserSession.UserId);
            ViewBag.IndustryList = IndustryList;
            ViewBag.CustomerTypeList = Contact_BAL.GetCustomerTypes();
            ViewBag.MeetingTypeList = Call_BAL.GetMeetingType();
            ViewBag.RatingList = Common_BAL.RatingDropDown();
            var CallTypeList = Call_BAL.GetCallType();
            ViewBag.CallTypeList = CallTypeList;

        }
        // GET: Member/Calls
        public ActionResult Index()
        {
            FillDropDown();
            return View();
        }
        [HttpPost]
        public JsonResult ManageCall(SP_Select_Call mdl, string IndustryTypeId)
        {
            Response_mdl response = new Response_mdl();
            bool IsAllowed = false;
            var UserRestriction = Common_BAL.VerifyUserAllowToPerformAction(En_RestrictionKey.Call.ToString(), UserSession.UserId, ref IsAllowed);
            if (!String.IsNullOrEmpty(mdl.FirstName))
            {
                if (!String.IsNullOrEmpty(IndustryTypeId))
                {
                    mdl.IndustryTypeId = Convert.ToInt64(IndustryTypeId);
                }
                //bool IsAleradyExists = Industry_BAL.IsIndustryAleradyExists(mdl.IndustryType, mdl.Id);
                if (mdl.CallId > 0)
                {

                    response = Call_BAL.VerifyMaster("Modify", mdl).Result;
                }
                else
                {
                    if (IsAllowed)
                    {
                        response = Call_BAL.VerifyMaster("Add", mdl).Result;
                    }
                    else {
                        response.IsSuccess = false;
                        response.Message = "You have reached your limit please contact to administrator.";
                    }
                }
                if (response != null)
                {

                    /*  ViewBag.Op_Message = response.Message;
                      if (response.IsSuccess && response.Id > 0)
                      {
                          ViewBag.Type = "1";
                          //return RedirectToAction(nameof(Index));
                      }
                      else
                      {
                          ViewBag.Type = "2";
                          //ModelState.AddModelError(String.Empty, response.Message);
                      }*/
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Name Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult CommonAction(string ActionType, string RefrenceId)
        {
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(RefrenceId))
            {
                //bool IsAleradyExists = Industry_BAL.IsIndustryAleradyExists(mdl.IndustryType, mdl.Id);
                if (ActionType == "MissCall")
                {
                    SP_Select_Call mdl = new SP_Select_Call();// { CallId = Convert.ToInt64(RefrenceId), IsCallNotReceived = true }
                    mdl = Call_BAL.GetLastCallHistory(Convert.ToInt64(RefrenceId));
                    mdl.IsCallNotReceived = true;
                    response = Call_BAL.AddCallHistory(mdl).Result;
                }
                if (response != null)
                {

                    /*ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }*/
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Reference Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }
        public ActionResult CallDetails(long id = 0)
        {
            FillDropDown();
            CallDetails_mdl mdl = new CallDetails_mdl();
            try
            {
                if (id == 0)
                {
                    return RedirectToAction("Index");
                }
                mdl = Call_BAL.GetLastCallDetail(mdl, UserSession.UserId, 0, id);
                if (mdl != null)
                {
                    List<SP_Select_Contact> select_Contacts = new List<SP_Select_Contact>();
                    select_Contacts = Contact_BAL.GetContacts(mdl.LastCall.ContactId, UserSession.UserId, 0);
                    mdl.Contacts = select_Contacts;
                    var Products = Contact_BAL.GetContactProduct(mdl.LastCall.ContactId);
                    mdl.Products = Products;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return View(mdl);
        }
        [HttpPost]
        public ActionResult CallDetails(CallDetails_mdl mdl)
        {
            Response_mdl response = new Response_mdl();
            if (mdl.LastCall.LastCallHistoryId > 0)
            {
                response = Call_BAL.UpdateCallDetails(mdl.LastCall).Result;
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Call_BAL.IsCallExists(id);
            if (IsExists)
            {
                var Op = Call_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
        public ActionResult CallHistory(long id = 0, long ContactId = 0, long CallId = 0)
        {
           List<SP_Select_Call> mdl = new List<SP_Select_Call>();
            try
            {
                mdl = Call_BAL.GetCallHistory(UserSession.UserId, CallId);
            }
            catch (Exception ex)
            {

            }
            return PartialView("_CallHistoryDetail", mdl);

        }

    }
}