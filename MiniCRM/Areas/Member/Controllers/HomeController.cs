﻿using CRMBAL;
using CRMModel;
using CRMModel.CustomEntity;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniCRM.Areas.Member.Controllers
{
    [Authorize(Roles ="Member")]
    public class HomeController : Controller
    {
        // GET: Member/Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult VerifyUserAllowToPerformAction(string ActionType,long RefrenceId)
        {
            SP_Select_UserPackageSubscription response = new SP_Select_UserPackageSubscription();
            bool IsAllowed = false;
            response=Common_BAL.VerifyUserAllowToPerformAction(ActionType, RefrenceId, ref IsAllowed);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}