﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRMBAL;
using CRMUtility;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
namespace MiniCRM.Areas.Member.Controllers
{
    public class OrganizationController : Controller
    {
        // GET: Member/Organization
        #region --Json Method--
        public JsonResult GetorganizationList()
        {
            var data = HttpContext.Request.QueryString;
            var list = Organization_BAL.GetorganizationJsGridList(data, UserSession.UserId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchOrganization(string SearchText)
        {
            var response = Organization_BAL.GetOrganisationAutoComplete(UserSession.UserId, SearchText);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [Authorize(Roles = "Member")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ManageOrganization(SP_Select_Organization mdl)
        {
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(mdl.OrganisationName))
            {
                bool IsAleradyExists = Organization_BAL.IsOrganisationAleradyExists(mdl.OrganisationName, mdl.Id,UserSession.UserId);
                if (mdl.Id > 0)
                {
                    if (!IsAleradyExists)
                    {
                        response = Organization_BAL.Update(mdl).Result;

                    }
                }
                else
                {
                    if (!IsAleradyExists)
                    {
                        response = Organization_BAL.Add(mdl).Result;
                    }
                }
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Organization Name Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Organization_BAL.IsOrganizationExists(id);
            if (IsExists)
            {
                var Op = Organization_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }
    }
}