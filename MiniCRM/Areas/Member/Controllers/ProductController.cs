﻿using CRMBAL;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniCRM.Areas.Member.Controllers
{
    [Authorize(Roles = "Member")]
    public class ProductController : Controller
    {
        #region --Json Method--
        public JsonResult GetProductList()
        {
            var data = HttpContext.Request.QueryString;
            var list = Product_BAL.GetProductJsGridList(data, UserSession.UserId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult FetchProduct(string SearchText)
        {
            var response = Product_BAL.GetProductAutoComplete(UserSession.UserId, SearchText);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
        // GET: Member/Product
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ManageProduct(SP_Select_Product mdl)
        {
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(mdl.ProductName))
            {
                bool IsAleradyExists = Product_BAL.IsProductAleradyExists(mdl.ProductName, mdl.Id);
                if (mdl.Id > 0)
                {
                    if (!IsAleradyExists)
                    {
                        response = Product_BAL.Update(mdl).Result;

                    }
                }
                else
                {
                    if (!IsAleradyExists)
                    {
                        response = Product_BAL.Add(mdl).Result;
                    }
                }
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Industry Type Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Industry_BAL.IsIndustryExists(id);
            if (IsExists)
            {
                var Op = Industry_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
        public ActionResult ManageContactProduct(long id = 0, long ContactId = 0, long CallId = 0)
        {
            return View();
        }
        public void FillDropDown()
        {

            ViewBag.RatingList = Common_BAL.RatingDropDown();

        }
        [HttpPost]
        public JsonResult ManageContactProduct(SP_Select_ContactProduct mdl)
        {
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(mdl.ProductName))
            {
                string Values = mdl.StrValue.ToString().Replace(",", "");
                mdl.Value = Convert.ToDecimal(Values);
                if (mdl.Id > 0)
                {
                    response = Product_BAL.VerifyMaster("Modify", mdl).Result;
                }
                else
                {
                    int ProductsCount = 0;
                    var Products = Contact_BAL.GetContactProduct(mdl.ConatctId);
                    if (Products != null)
                    {
                        ProductsCount = Products.Count();
                    }
                    if (ProductsCount < 4)
                    {
                        response = Product_BAL.VerifyMaster("Add", mdl).Result;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.IsErrorOccur = true;
                        response.Message = "You can only add 4 product";

                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.IsErrorOccur = true;
                response.Message = "Product Name Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult ContactProduct(long id = 0, long ContactId = 0, long CallId = 0)
        {
            FillDropDown();
            SP_Select_ContactProduct mdl = new SP_Select_ContactProduct();
            try
            {
                if (id > 0)
                {
                    var Products = Contact_BAL.GetContactProduct(ContactId, id);
                    if (Products != null)
                    {
                        if (Products.Count() > 0)
                        {
                            mdl = Products.FirstOrDefault();
                        }
                    }
                }
                else
                {
                    mdl.ConatctId = ContactId;
                    mdl.CallId = CallId;
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("_CreateOrUpdateContactProduct", mdl);
        }
        public ActionResult ContactProductDetails(long id = 0, long ContactId = 0)
        {
            List<SP_Select_ContactProduct> mdl = new List<SP_Select_ContactProduct>();
            try
            {
                mdl = Contact_BAL.GetContactProduct(ContactId);
            }
            catch (Exception ex)
            {

            }
            return PartialView("_ContactProductDetails", mdl);
        }



    }
}