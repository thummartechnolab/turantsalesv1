﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRMBAL;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;

namespace MiniCRM.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,SupperAdmin")]
    public class UserController : Controller
    {

        #region --Json Method--
        public JsonResult GetUsersList()
        {
            var data = HttpContext.Request.QueryString;
            var list = User_BAL.GetUserJsGridList(data);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion
        // GET: Admin/User
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult Delete(long id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = User_BAL.UserExists(id);
            if (IsExists)
            {
                var Op = User_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CommonAction(int UserId, bool CurrentValue, string ActionType)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = User_BAL.UserExists(UserId);
            if (IsExists)
            {
                var Op = User_BAL.CommonAction(UserId, CurrentValue, ActionType);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public void FillDropDown(long id = 0)
        {
            var PackageList = CRMBAL.Package_BAL.GetPackages();
            if (PackageList.Where(p => p.Id == id).Select(p => p.PackageName).FirstOrDefault() == "Free")
            {
                ViewBag.Packages = PackageList;
            }
            else
            {
                ViewBag.Packages = PackageList.Where(p => p.PackageName != "Free").ToList();
            }
            List<SelectListItem> items = new List<SelectListItem>();
            items = Common_BAL.GenderDropdown();
            ViewBag.GenderList = items;
        }
        public ActionResult Userdetail(long id = 0)
        {
            SP_Select_User mdl = new SP_Select_User();
            if (id > 0)
            {
                mdl = User_BAL.GetUserListById(id).FirstOrDefault();
            }
            FillDropDown(mdl.PackageId);
            return View(mdl);
        }
        [HttpPost]
        public ActionResult Userdetail(SP_Select_UserDetails mdl)
        {
            Response_mdl response = new Response_mdl();
            if (mdl.id > 0)
            {
                var userdetail = User_BAL.Update(mdl);
                response = userdetail.Result;
                if (response != null)
                {
                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                    }
                    else
                    {
                        ViewBag.Type = "2";
                    }
                }
            }
            return RedirectToAction("Index");
        }
    }
}