﻿using CRMBAL;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniCRM.Areas.Admin.Controllers
{
    public class AnnouncementController : Controller
    {
        // GET: Admin/Announcement
        [Authorize(Roles = "Admin,SupperAdmin")]
        public ActionResult Index()
        {
            return View();
        }
        #region --Json Method--
        public JsonResult GetAnnouncementList()
        {
            var data = HttpContext.Request.QueryString;
            var list = Announcement_BAL.GetAnnouncementJsGridList(data, UserSession.UserId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion
        public ActionResult ManageAnnouncement(long id = 0)
        {
            SP_Select_Announcement mdl = new SP_Select_Announcement();
            if (id > 0)
            {
                mdl = Announcement_BAL.GetAnnouncementByID(id).FirstOrDefault();
            }
            return View(mdl);
        }

        [HttpPost]
        public ActionResult ManageAnnouncement(SP_Select_Announcement mdl)
        {
            Response_mdl response = new Response_mdl();
            if (ModelState.IsValid)
            {
                if (mdl.Id > 0)
                {
                    response = Announcement_BAL.Update(mdl).Result;

                }
                else
                {
                    response = Announcement_BAL.Add(mdl).Result;
                }
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                    }
                    else
                    {
                        ViewBag.Type = "2";
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Annoucement Required";
                return View(mdl);
            }

            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Announcement_BAL.IsAnnoucementExists(id);
            if (IsExists)
            {
                var Op = Announcement_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
    }
}