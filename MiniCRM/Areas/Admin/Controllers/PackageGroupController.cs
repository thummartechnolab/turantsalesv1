﻿using CRMBAL;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRMModel;
namespace MiniCRM.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,SupperAdmin")]
    public class PackageGroupController : Controller
    {
        // GET: Admin/PackageGroup
        #region --Json Method--
        public JsonResult GetPackageGroupList()
        {
            var data = HttpContext.Request.QueryString;
            var list = PackageGroup_BAL.GetPackageGroupJsGridList(data, 0);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GroupDetails(long id)
        {
            PackageGroupDetails_mdl mdl = new PackageGroupDetails_mdl();
            var configList = PackageGroup_BAL.GetPackageGroupConfigurations(0, id);
            mdl.PackageGroupConfigurations = configList;
            return View(mdl);
        }
        public void FillDropDown(SP_Select_PackageGroupConfiguration mdl)
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            List<string> keys = new List<string>();
            keys = Enum.GetNames(typeof(En_RestrictionKey)).ToList();
            selectListItems = keys.Select(s => new SelectListItem { Text =s, Value = s,Selected=(String.IsNullOrEmpty(mdl.RestrictionKey)?false:(mdl.RestrictionKey==s)?true:false) }).ToList();
            ViewBag.RestrictionKeyList = selectListItems;
            var Dependancy = Enum.GetNames(typeof(En_RestrictionDependency)).ToList();
            // selectListItems = new List<SelectListItem>();
            var selectList = Dependancy.Select(s => new SelectListItem { Text = s, Value = s }).ToList();
            ViewBag.RestrictionDependencyList = selectList;
        }
        public ActionResult ManageConfigurationPartial(long id=0,long PackageGroupId=0)
        {
            SP_Select_PackageGroupConfiguration mdl = new SP_Select_PackageGroupConfiguration();
            try
            {
                if(id>0)
                {
                    var configList = PackageGroup_BAL.GetPackageGroupConfigurations(id,PackageGroupId);
                    if (configList != null)
                    {
                        mdl = configList.FirstOrDefault();
                        mdl.RestrictionKey = mdl.RestrictionKey.Trim();
                        mdl.RestrictionDependency = mdl.RestrictionDependency.Trim();
                    }
                }
                else
                {
                    mdl.PackageGroupId = PackageGroupId;
                }
            }
            catch (Exception ex)
            {


            }
            FillDropDown(mdl);
            return PartialView("_ManageConfiguration", mdl);
        }

        [HttpPost]
        public JsonResult ManageConfiguration(SP_Select_PackageConfiguration mdl)
        {
           
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(mdl.RestrictionKey))
            {
                bool IsAleradyExists = PackageConfiguration_BAL.IsPackageAleradyExists(mdl.RestrictionKey, mdl.Id);
                if (mdl.Id > 0)
                {
                    if (IsAleradyExists)
                    {
                        response = PackageConfiguration_BAL.Update(mdl).Result;

                    }
                }
                else
                {
                    if (!IsAleradyExists)
                    {
                        response = PackageConfiguration_BAL.Add(mdl).Result;
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Package Group Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult ManagePackageGroup(SP_Select_PackageGroup mdl)
        {
            Response_mdl response = new Response_mdl();
            if (!String.IsNullOrEmpty(mdl.PackageGroupName))
            {
                bool IsAleradyExists = PackageGroup_BAL.IsPackageGroupAleradyExists(mdl.PackageGroupName, mdl.Id);
                if (mdl.Id > 0)
                {
                    if (!IsAleradyExists)
                    {
                        response = PackageGroup_BAL.Update(mdl).Result;

                    }
                }
                else
                {
                    if (!IsAleradyExists)
                    {
                        response = PackageGroup_BAL.Add(mdl).Result;
                    }
                }
                /*if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                        //return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ViewBag.Type = "2";
                        //ModelState.AddModelError(String.Empty, response.Message);
                    }
                }*/
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Package Group Required";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = PackageGroup_BAL.IsPackageGroupExists(id);
            if (IsExists)
            {
                var Op = PackageGroup_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }

        

    }
}