﻿using CRMBAL;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiniCRM.Areas.Admin.Controllers
{
    public class PackageController : Controller
    {
        // GET: Admin/Package
        #region --Json Method--
        public JsonResult GetPackagList()
        {
            var data = HttpContext.Request.QueryString;
            var list = Package_BAL.GetPackageJsGridList(data, UserSession.UserId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion
        [Authorize(Roles = "Admin,SupperAdmin")]
        public ActionResult Index()
        {
            return View();
        }

        public void FillDropDown()
        {
            var packageGroups = Package_BAL.GetPackageGroup();
            ViewBag.PackageGroupList = packageGroups;

        }
        public ActionResult ManagePackage(int id = 0)
        {
            SP_Select_Package mdl = new SP_Select_Package();
            FillDropDown();
            if (id > 0)
            {
                mdl = Package_BAL.GetPackageByID(id).FirstOrDefault();
            }

            return View(mdl);
        }

        [HttpPost]
        public ActionResult ManagePackage(SP_Select_Package mdl)
        {
            Response_mdl response = new Response_mdl();
            if (ModelState.IsValid)
            {
                if (mdl.Id > 0)
                {
                    response = Package_BAL.Update(mdl).Result;
                }
                else
                {
                    response = Package_BAL.Add(mdl).Result;
                }
                if (response != null)
                {

                    ViewBag.Op_Message = response.Message;
                    if (response.IsSuccess && response.Id > 0)
                    {
                        ViewBag.Type = "1";
                    }
                    else
                    {
                        ViewBag.Type = "2";
                    }
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Package Name Required";
                return View(mdl);
            }

            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            bool IsExists = Package_BAL.IsPackageExists(id);
            if (IsExists)
            {
                var Op = Package_BAL.Delete(id);
                response = Op.Result;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = "Record Not Found.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("Index");
        }
    }
}