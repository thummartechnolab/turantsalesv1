﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMModel.CustomEntity;
namespace CRMModel.ViewModel
{
    public class CallDetails_mdl
    {
        public List<SP_Select_Contact> Contacts { get; set; }
        public List<SP_Select_ContactProduct> Products { get; set; }
        public string ProductName1 { get; set; }
        public decimal Unit1 { get; set; }
        public decimal Value1 { get; set; }
        public int Rating1 { get; set; }
        public string ProductName2 { get; set; }
        public decimal Unit2 { get; set; }
        public decimal Value2 { get; set; }
        public int Rating2 { get; set; }
        public string ProductName3 { get; set; }
        public decimal Unit3 { get; set; }
        public decimal Value3 { get; set; }
        public int Rating3 { get; set; }
        public string ProductName4 { get; set; }
        public decimal Unit4 { get; set; }
        public decimal Value4 { get; set; }
        public int Rating4 { get; set; }
        public int CustomerTypeId { get; set; }
        public int MeetingTypeId { get; set; }
        public int CallTypeId { get; set; }
        public string CallOpenDate { get; set; }
        public string Days { get; set; }
        public string NextDate { get; set; }
        public bool IsNoResponse { get; set; }
        public string Remark { get; set; }
        public long LastCallHistoryId { get; set; }
        public long CallId { get; set; }
        public SP_Select_Call LastCall { get; set; }
    }
}
