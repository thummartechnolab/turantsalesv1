﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.ViewModel
{
    public class Response_mdl
    {
        public long Id { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsErrorOccur { get; set; }
        public string Message { get; set; }
        public Object Data { get; set; }
    }
}
