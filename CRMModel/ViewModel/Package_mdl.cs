﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel
{
    public class Package_mdl
    {
        public int Id { get; set; }
        public int PackageGroupId { get; set; }

        public string PackageName { get; set; }
        public int ValidityMonths { get; set; }
        public int ValidityInDays { get; set; }
        public decimal Price { get; set; }
        public string CurrencyCode { get; set; }
        public bool IsAnyRestriction { get; set; }
        public bool IsTaxablePrice { get; set; }
        public long CreadtedUserId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public long UpdatedUserId { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
