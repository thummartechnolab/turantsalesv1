﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.ViewModel
{
    public class User_mdl
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string ContactNo { get; set; }
        public int PackageId { get; set; }
        public string UserUniqueId { get; set; }
        public long TaxId { get; set; }
        public int UserTypeId { get; set; }

    }
}
