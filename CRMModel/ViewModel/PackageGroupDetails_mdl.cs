﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMModel.CustomEntity;
namespace CRMModel.ViewModel
{
    public class PackageGroupDetails_mdl
    {
        public long Id { get; set; }
        public List<SP_Select_PackageGroupConfiguration> PackageGroupConfigurations { get; set; }
    }
}
