﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.ViewModel
{
    public class Industry_mdl
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string IndustryType { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
    }
}
