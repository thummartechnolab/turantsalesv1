﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_UserDetails
    {
        public int id { get; set; }
        public long UserUniqueId { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string WorkEmail { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        [Display(Name = "Contact No")]
        public string ContactNo { get; set; }
        public int PackageId { get; set; }
        public bool Isactive { get; set; }
    }
}
