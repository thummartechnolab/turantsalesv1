﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_Dashboard
    {
        public int TotalUser { get; set; }
        public int TotalPaidUser { get; set; }
        public int TotalUnPaidUser { get; set; }
        public int TodayNewUser { get; set; }
    }
}
