﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_Announcement
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        [Required]
        public string AnnouceMent { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsMember { get; set; }
    }
   
}
