﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_Industry
    {

        public long Id { get; set; }
        public long UserId { get; set; }
        public string IndustryType { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }
    }
    public class SP_Select_Organization
    {

        public long Id { get; set; }
        public long UserId { get; set; }
        public long IndustryType { get; set; }
        public string OrganisationName { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }
    }
    public class SP_Select_Designation
    {

        public long Id { get; set; }
        public long UserId { get; set; }
        public string Designation { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }
    }
    public class SP_Select_City
    {

        public long Id { get; set; }
        public long UserId { get; set; }
        public string CityName { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }
    }
}
