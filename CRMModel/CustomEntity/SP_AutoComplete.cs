﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_AutoComplete
    {
        public long value { get; set; }
        public string label { get; set; }
    }
}
