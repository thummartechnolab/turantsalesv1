﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_Call
    {
        public long ContactId { get; set; }
        public long LastCallHistoryId { get; set; }
        public long CallId { get; set; }
        public long CityId { get; set; }
        public long FirstCallProductId { get; set; }
        public long ProductId { get; set; }
        public long DesignationId { get; set; }
        public long OrganisationId { get; set; }
        public int CustomerTypeId { get; set; }
        public string OrganisationName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhoneNo { get; set; }
        public string Extn { get; set; }
        public string PhoneNo2 { get; set; }
        public string Designation { get; set; }
        public string ProductName { get; set; }
        public string CityName { get; set; }
        public string CustomerType { get; set; }
        public string CustomerTypeCode { get; set; }
        public int DaysPassed { get; set; }
        public string NextCallDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsCallNotReceived { get; set; }
        public string CallDate { get; set; }
        public int CallTypeId { get; set; }
        public string CallType { get; set; }
        public int MeetingTypeId { get; set; }
        public string MeetingType { get; set; }
        public string CallRemark { get; set; }
        public string MissCallHistory { get; set; }
        public long IndustryTypeId { get; set; }
        public string IndustryType { get; set; }
        public string MissCall { get; set; }
    }
    
}
