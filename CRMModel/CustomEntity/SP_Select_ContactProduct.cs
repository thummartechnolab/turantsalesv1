﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_ContactProduct
    {
        public long Id { get; set; }
        public long CallId { get; set; }
        public long ConatctId { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Unit { get; set; }
        public string StrValue { get; set; }
        public decimal Value { get; set; }
        public int Rating { get; set; }
    }
}
