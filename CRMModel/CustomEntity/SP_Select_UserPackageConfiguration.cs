﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_PackageConfiguration
    {
        public long Id { get; set; }
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public bool IsAnyRestriction { get; set; }
        public string RestrictionKey { get; set; }
        public string RestrictionValue { get; set; }
        public string RestrictionDependency { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedOn { get; set; }
        public string CreatedOn { get; set; }
        
    }
}
