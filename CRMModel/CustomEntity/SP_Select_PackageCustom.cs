﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_PackageCustom: SP_Select_Package
    {
        public List<SP_Select_PackageGroupConfiguration> PackageGroupConfigurations { get; set; }
    }
}
