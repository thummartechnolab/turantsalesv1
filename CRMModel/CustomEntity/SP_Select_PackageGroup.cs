﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_PackageGroup
    {
        public long Id { get; set; }
        public int PackageTypeId { get; set; }
        public string PackageType { get; set; }
        public string PackageGroupName { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }

    }
}
