﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_User
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string WorkEmail { get; set; }
        public string Gender { get; set; }
        public string ContactNo { get; set; }
        public int UserTypeId { get; set; }
        public string UserType { get; set; }
        public string UserTypeCode { get; set; }
        public long SubscriptionId { get; set; }
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public decimal TotalPaidAmount { get; set; }
        public bool IsPaid { get; set; }
        public string SubscriptionDate { get; set; }
        public string SubscriptionExpireDate { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string UpdatedDateTime { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
