﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_UserPackageSubscription
    {
        public string RestrictionKey { get; set; }
        public string RestrictionValue { get; set; }
        public long RestrictionRowId { get; set; }
        public string RestrictionRowName { get; set; }
        public int RestrictionRowCount { get; set; }
        public bool IsLimitOver { get; set; }
        public bool IsAllowed { get; set; }
    }
}
