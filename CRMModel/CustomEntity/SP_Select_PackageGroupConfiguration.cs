﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_PackageGroupConfiguration
    {
        public long Id { get; set; }
        public long PackageGroupId { get; set; }
        public string RestrictionKey { get; set; }
        public string RestrictionValue { get; set; }
        public string RestrictionDependency { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public bool IsActive { get; set; }
    }
}
