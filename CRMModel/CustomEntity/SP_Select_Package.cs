﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_Package
    {
        public int Id { get; set; }
        [Required]
        public int PackageGroupId { get; set; }
        public long TaxId { get; set; }
        public string PackageGroupName { get; set; }
        [Required]
        public string PackageName { get; set; }
        [Required]
        public int ValidityMonths { get; set; }
        [Required]
        public int ValidityInDays { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public string CurrencyCode { get; set; }
        public string TaxName { get; set; }
        public decimal TaxPercentage { get; set; }
        public bool IsAnyRestriction { get; set; }
        public bool IsTaxablePrice { get; set; }
        public bool IsActive { get; set; }
        public bool IsDisplay { get; set; }

    }
}
