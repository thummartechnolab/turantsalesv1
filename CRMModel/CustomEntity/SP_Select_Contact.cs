﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel.CustomEntity
{
    public class SP_Select_Contact
    {
        public long Id { get; set; }
        public long OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhoneNo { get; set; }
        public string PhoneNo2 { get; set; }
        public string Extn { get; set; }
        public long DesignationId { get; set; }
        public string Designation { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
        public string UnitNo { get; set; }
        public string Street { get; set; }
        public string PinCode { get; set; }
        public string PrimaryEmail { get; set; }
        public string ScondaryEmail { get; set; }
        public string Chat1 { get; set; }
        public string Chat2 { get; set; }
        public long ParentContactId { get; set; }
        public int CustomerTypeId { get; set; }
        public string CustomerType { get; set; }
        public string CustomerTypeCode { get; set; }
        public long IndustryTypeId { get; set; }
        public string IndustryType { get; set; }

    }
}
