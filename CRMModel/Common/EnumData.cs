﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMModel
{
    public enum En_ApiResponse
    {
        IsSuccess,
        Message, Error,
        Data

    }
    public enum En_EntityType
    {
        List,
        FirtOrDefault

    }
    public enum En_EntityRequestType
    {
        ViewModel,
        ApiModel,
        EntityModel

    }

    public enum En_CommonKeys
    {
        UserId,
        UserUId

    }
    public enum En_Gender
    {
        Male,
        Female
    }
    public enum En_Roles
    {
        Admin,
        Member,
        SupperAdmin
    }
    public enum En_UserTypeCode
    {
        SA,
        AA,
        MR
    }
    public enum En_Folder
    {
        Upload
    }
    public enum En_RestrictionKey
    {
        Contact,
        Call,
        IndustryType
    }
    public enum En_RestrictionDependency
    {
        None,
        IndustryType
    }
    public enum En_Session
    {
        StartTime
    }
}
