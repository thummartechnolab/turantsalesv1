﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;

namespace CRMBAL
{
    public class Announcement_BAL
    {
        #region ---Grid Selection---
        public static Task<SP_Select_Announcement> GetFilter(NameValueCollection filter)
        {
            SP_Select_Announcement Announce = new SP_Select_Announcement()
            {

                AnnouceMent = filter["AnnouceMent"]
            };
            return Task.FromResult<SP_Select_Announcement>(Announce);
        }
        public static List<SP_Select_Announcement> GetAnnouncementJsGridList(NameValueCollection objfilter, long UserId, string UserTypeCode = "AA")
        {
            List<SP_Select_Announcement> select_Announcement = new List<SP_Select_Announcement>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Announcement");
                var A = new SqlParameter("@A", "AnnouncementList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_Announcement = ctx.Database.SqlQuery<SP_Select_Announcement>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                select_Announcement = select_Announcement.Where(c => c.AnnouceMent.ToLower().Contains(filter.AnnouceMent.ToLower()) || String.IsNullOrEmpty(filter.AnnouceMent)).ToList();
            }

            return select_Announcement;

        }
        public static List<SP_Select_Announcement> GetAnnouncementByID(long Id) {
            List<SP_Select_Announcement> select_Announcement = new List<SP_Select_Announcement>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Announcement");
                var A = new SqlParameter("@A", "AnnouncementList");
                var B = new SqlParameter("@B", Id);
                select_Announcement = ctx.Database.SqlQuery<SP_Select_Announcement>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
            }

            return select_Announcement;
        }
        public static List<SP_Select_Announcement> GetAnnouncementsList(string Type)
        {
            List<SP_Select_Announcement> select_Announcement = new List<SP_Select_Announcement>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Announcement");
                var A = new SqlParameter("@A", "AnnouncementList");
                var B = new SqlParameter("@B", UserSession.UserId.ToString());
                select_Announcement = ctx.Database.SqlQuery<SP_Select_Announcement>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                if(Type=="Common")
                {
                    select_Announcement = select_Announcement.Where(s => s.IsMember == false).ToList();
                }

            }

            return select_Announcement;

        }
        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_Announcement mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var AnoMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "Announcement", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    Announcement_Master _Master = new Announcement_Master();
                    _Master = (Announcement_Master)AnoMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.UserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Announcement_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Announcement mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var Annous = db.Announcement_Master.Where(s => s.id == mdl.Id).FirstOrDefault();
                    Annous.AnnouceMent = mdl.AnnouceMent;
                    Annous.IsActive = mdl.IsActive;
                    Annous.IsMember = mdl.IsMember;
                    string DateAndTime = Common.GetCurrentDateTime();
                    Annous.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    Annous.UpdatedUserId = UserSession.UserId;
                    db.Entry(Annous).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = Annous.id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = Annous.id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = Annous.id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var announcement = db.Announcement_Master.Where(s => s.id == id).FirstOrDefault();
                    announcement.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    announcement.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    announcement.UpdatedUserId = UserSession.UserId;
                    db.Entry(announcement).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = announcement.id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        public static bool IsAnnoucementExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.Announcement_Master.Any(e => e.id == id);
        }
    }
}
