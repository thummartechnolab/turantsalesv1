﻿using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMBAL
{
    public class Call_BAL
    {
        #region ---Select Method---
        public static List<SP_Select_Call> GetCallList(long UserId, long IndustryTypeId = 0, long ContactId = 0, long CallId = 0)
        {
            List<SP_Select_Call> select_Calls = new List<SP_Select_Call>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Call");
                var A = new SqlParameter("@A", "CallList");
                var B = new SqlParameter("@B", UserId.ToString());
                var C = new SqlParameter("@C", (IndustryTypeId > 0 ? IndustryTypeId.ToString() : "0"));
                var D = new SqlParameter("@D", (ContactId > 0 ? ContactId.ToString() : "0"));
                var E = new SqlParameter("@E", (CallId > 0 ? CallId.ToString() : "0"));
                var F = new SqlParameter("@F", Common.GetCurrentDateTime());
                select_Calls = ctx.Database.SqlQuery<SP_Select_Call>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C,@D,@E,@F", Module, A, B, C, D, E,F).ToList();
            }
            return select_Calls;

        }
        public static List<SP_Select_Call> GetCallHistoryList(long UserId, long CallId = 0)
        {
            List<SP_Select_Call> select_Calls = new List<SP_Select_Call>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Call");
                var A = new SqlParameter("@A", "CallHistory");
                var B = new SqlParameter("@B", UserId.ToString());
                var C = new SqlParameter("@C", (CallId > 0 ? CallId.ToString() : "0"));
                var D = new SqlParameter("@D", Common.GetCurrentDateTime());
                select_Calls = ctx.Database.SqlQuery<SP_Select_Call>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C,@D", Module, A, B, C,D).ToList();
            }
            return select_Calls;

        }
        public static SP_Select_Call GetLastCallHistory(long CallId = 0)
        {
            SP_Select_Call select_Calls = new SP_Select_Call();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Call");
                var A = new SqlParameter("@A", "GetCallhistoryByCallId");
                var B = new SqlParameter("@B", (CallId > 0 ? CallId.ToString() : "0"));
                select_Calls = ctx.Database.SqlQuery<SP_Select_Call>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).FirstOrDefault();
            }
            return select_Calls;

        }
        public static List<CallType_Master> GetCallType()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<CallType_Master> customerTypes = new List<CallType_Master>();
            customerTypes = db.CallType_Master.Where(s => s.IsActive == true && s.IsDeleted == false).ToList();
            return customerTypes;
        }
        public static List<MeetingType_Master> GetMeetingType()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<MeetingType_Master> customerTypes = new List<MeetingType_Master>();
            customerTypes = db.MeetingType_Master.Where(s => s.IsActive == true && s.IsDeleted == false).ToList();
            return customerTypes;
        }
        public static CallDetails_mdl GetLastCallDetail(CallDetails_mdl mdl, long UserId, long ContactId, long CallId)
        {
            var selectCall = GetCallList(UserId, 0, ContactId, CallId);
            if (selectCall != null)
            {
                if (selectCall.Count() > 0)
                {
                    var lastCall = selectCall.FirstOrDefault();
                    if (lastCall != null)
                    {
                        mdl.LastCall = lastCall;
                    }
                }
            }
            return mdl;
        }
        public static List<SP_Select_Call> GetCallHistory(long UserId, long CallId)
        {
            var selectCall = GetCallHistoryList(UserId, CallId);
            //mdl = selectCall;
            return selectCall;
        }
        #endregion
        #region ---Grid Selection---
        public static Task<SP_Select_Call> GetFilter(NameValueCollection filter)
        {
            SP_Select_Call call = new SP_Select_Call()
            {
                OrganisationName = filter["OrganisationName"],
                FirstName = filter["FirstName"],
                LastName = filter["LastName"],
                PrimaryPhoneNo = filter["PrimaryPhoneNo"],
                PhoneNo2 = filter["PhoneNo2"],
                Designation = filter["Designation"],
                ProductName = filter["ProductName"],
                CityName = filter["CityName"],
                CustomerType = filter["CustomerType"]
            };
            return Task.FromResult<SP_Select_Call>(call);
        }
        public static List<SP_Select_Call> GetCallJsGridList(NameValueCollection objfilter, long UserId, long IndustryTypeId, string UserTypeCode = "AA")
        {
            List<SP_Select_Call> select_Calls = new List<SP_Select_Call>();
            var filter = GetFilter(objfilter).Result;
            select_Calls = GetCallList(UserId, IndustryTypeId);
            select_Calls = select_Calls.Where(c => (String.IsNullOrEmpty(filter.OrganisationName) || c.OrganisationName.Contains(filter.OrganisationName)) && (String.IsNullOrEmpty(filter.FirstName) || c.FirstName.Contains(filter.FirstName)) && (String.IsNullOrEmpty(filter.LastName) || c.LastName != null && c.LastName.Contains(filter.LastName)) && (String.IsNullOrEmpty(filter.PrimaryPhoneNo) || c.PrimaryPhoneNo.Contains(filter.PrimaryPhoneNo)) && (String.IsNullOrEmpty(filter.PhoneNo2) || c.PhoneNo2.Contains(filter.PhoneNo2)) && (String.IsNullOrEmpty(filter.Designation) || c.Designation.Contains(filter.Designation)) && (String.IsNullOrEmpty(filter.ProductName) || c.ProductName.Contains(filter.ProductName)) && (String.IsNullOrEmpty(filter.CityName) || c.CityName.Contains(filter.CityName)) && (String.IsNullOrEmpty(filter.CustomerType) || c.CustomerTypeCode.Contains(filter.CustomerType))).ToList();
            return select_Calls;

        }
        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> VerifyMaster(string Type, SP_Select_Call mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                long UserId = UserSession.UserId;
                var Organization = Organization_BAL.GetOrganisationDetail(mdl.OrganisationName.Trim());
                if (Organization != null)
                {
                    mdl.OrganisationId = Organization.Id;
                }
                else
                {
                    SP_Select_Organization org = new SP_Select_Organization() { OrganisationName = mdl.OrganisationName.Trim(), IsActive = true };
                    var orgResponse = Organization_BAL.Add(org).Result;
                    if (orgResponse.IsSuccess)
                    {
                        mdl.OrganisationId = orgResponse.Id;
                    }
                }
                var Designation = Designation_BAL.GetDesignationDetail(mdl.Designation.Trim());
                if (Designation != null)
                {
                    mdl.DesignationId = Designation.Id;
                }
                else
                {
                    SP_Select_Designation _Designation = new SP_Select_Designation() { Designation = mdl.Designation.Trim(), IsActive = true };
                    var dsgResponse = Designation_BAL.Add(_Designation).Result;
                    if (dsgResponse.IsSuccess)
                    {
                        mdl.DesignationId = dsgResponse.Id;
                    }
                }
                var City = City_BAL.GetCityDetail(mdl.CityName.Trim());
                if (City != null)
                {
                    mdl.CityId = City.Id;
                }
                else
                {
                    SP_Select_City _City = new SP_Select_City() { CityName = mdl.CityName.Trim(), IsActive = true };
                    var ctyResponse = City_BAL.Add(_City).Result;
                    if (ctyResponse != null)
                    {
                        mdl.CityId = ctyResponse.Id;
                    }
                }

                mdl.CustomerTypeId = 0;
                /*  var customerType = Contact_BAL.GetCustomerTypeByTypeCode(mdl.CustomerTypeCode);
                  if (customerType != null)
                  {
                      mdl.CustomerTypeId = customerType.Id;
                  }*/

                var Contact = Contact_BAL.GetContact(mdl.PrimaryPhoneNo.Trim());
                if (Contact != null)
                {
                    mdl.ContactId = Contact.Id;

                    if (Type == "Modify")
                    {
                        var cntResponse = Contact_BAL.Update(mdl).Result;
                    }
                    else if (Contact.IndustryTypeId == null || Contact.IndustryTypeId == 0)
                    {
                        var cntResponse = Contact_BAL.Update(mdl).Result;
                    }
                }
                else
                {
                    var cntResponse = Contact_BAL.Add(mdl).Result;
                    if (cntResponse.IsSuccess)
                    {
                        mdl.ContactId = cntResponse.Id;
                    }
                }
                var Product = Product_BAL.GetProductDetail(mdl.ProductName.Trim());
                if (Product != null)
                {
                    mdl.ProductId = Product.Id;
                }
                else
                {
                    SP_Select_Product _Product = new SP_Select_Product() { ProductName = mdl.ProductName.Trim(), IsActive = true };
                    var prdResponse = Product_BAL.Add(_Product).Result;
                    if (prdResponse.IsSuccess)
                    {
                        mdl.ProductId = prdResponse.Id;
                    }
                }
                if (Type == "Add")
                {
                    response = Call_BAL.Add(mdl).Result;
                }
                else if (Type == "Modify")
                {
                    response = Call_BAL.Update(mdl).Result;
                }
            }
            catch (Exception ex)
            {


            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Add(SP_Select_Call mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    string DateAndTime = Common.GetCurrentDateTime();
                    string CurrentTime = Common.GetCurrentDateTime("Time");
                    Call_Master call = new Call_Master();
                    call.UserId = UserSession.UserId;
                    call.ContactId = mdl.ContactId;
                    call.CreatedUserId = UserSession.UserId;
                    call.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    call.IsActive = true; call.IsDeleted = false;
                    db.Call_Master.Add(call);
                    if (db.SaveChanges() > 0)
                    {
                        Call_History history = new Call_History();
                        history.CallId = call.Id;
                        history.CallDate = Convert.ToDateTime(DateAndTime);
                        if (!String.IsNullOrEmpty(mdl.NextCallDate))
                        {
                            history.NextCallDate = Convert.ToDateTime(mdl.NextCallDate + " " + CurrentTime);
                        }
                        history.IsActive = true;
                        history.IsDeleted = false;
                        history.IsCallNotReceived = false;
                        history.CreatedUserId = UserSession.UserId;
                        history.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                        db.Call_History.Add(history);
                        ContactProduct_Master contactProduct = new ContactProduct_Master();
                        contactProduct.ProductId = mdl.ProductId;
                        contactProduct.CallId = call.Id;
                        contactProduct.ConatctId = mdl.ContactId;
                        contactProduct.IsMainProduct = true;
                        contactProduct.CreadtedUserId = UserSession.UserId;
                        contactProduct.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                        contactProduct.IsActive = true;
                        contactProduct.IsDeleted = false;
                        db.ContactProduct_Master.Add(contactProduct);
                        // db.IndustryType_Master.Add(_Master);
                        if (db.SaveChanges() > 0)
                        {

                            response.IsSuccess = true;
                            response.Id = call.Id;
                            response.Message = "Added Successfully";
                            mdl.CallId = call.Id;
                            mdl.FirstCallProductId = contactProduct.Id;
                            mdl.LastCallHistoryId = history.Id;
                            response.Data = mdl;
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Id = call.Id;
                            response.Message = "Rows not affected.";
                            //mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                            //mdl.UpdatedOn = "";
                            mdl.CallId = call.Id;
                            mdl.FirstCallProductId = 0;
                            mdl.LastCallHistoryId = 0;
                            response.Data = mdl;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Call mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    string DateAndTime = Common.GetCurrentDateTime();
                    string CurrentTime = Common.GetCurrentDateTime("Time");
                    var OldCallHistory = db.Call_History.Where(s => s.Id == mdl.LastCallHistoryId).FirstOrDefault();
                    if (OldCallHistory != null)
                    {
                        OldCallHistory.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                        OldCallHistory.UpdatedUserId = UserSession.UserId;
                        db.Entry(OldCallHistory).State = EntityState.Modified;
                    }
                    Call_History history = new Call_History();
                    history.CallId = mdl.CallId;
                    history.CallDate = Convert.ToDateTime(DateAndTime);
                    if (!String.IsNullOrEmpty(mdl.NextCallDate))
                    {
                        string Nextdate = mdl.NextCallDate + " " + CurrentTime;
                        history.NextCallDate = Convert.ToDateTime(Nextdate);////, System.Globalization.CultureInfo.InvariantCulture
                        //history.NextCallDate = Convert.ToDateTime(mdl.NextCallDate + " " + CurrentTime);
                    }
                    history.IsActive = true;
                    history.IsDeleted = false;
                    history.IsCallNotReceived = false;
                    history.CreatedUserId = UserSession.UserId;
                    history.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Call_History.Add(history);

                    //ContactProduct_Master contactProduct = new ContactProduct_Master();
                    var contactProduct = db.ContactProduct_Master.Where(s => s.Id == mdl.FirstCallProductId).FirstOrDefault();
                    if (contactProduct != null)
                    {
                        contactProduct.ProductId = mdl.ProductId;
                        contactProduct.IsMainProduct = true;
                        contactProduct.UpdatedUserId = UserSession.UserId;
                        contactProduct.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                        db.Entry(contactProduct).State = EntityState.Modified;
                    }
                    // db.IndustryType_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = mdl.CallId;
                        response.Message = "Added Successfully";
                        mdl.CallId = mdl.CallId;
                        mdl.FirstCallProductId = contactProduct.Id;
                        mdl.LastCallHistoryId = history.Id;
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = mdl.CallId;
                        response.Message = "Rows not affected.";
                        //mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        //mdl.UpdatedOn = "";
                        mdl.CallId = mdl.CallId;
                        mdl.FirstCallProductId = 0;
                        mdl.LastCallHistoryId = 0;
                        response.Data = mdl;
                    }

                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }

        public static Task<Response_mdl> UpdateCallDetails(SP_Select_Call mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    string DateAndTime = Common.GetCurrentDateTime();
                    var select_Contacts = Contact_BAL.GetContacts(mdl.ContactId, UserSession.UserId, 0);
                    long[] ContactIds = select_Contacts.Select(s => s.Id).ToArray();
                    var contact = db.Contact_Master.Where(s => ContactIds.Contains(s.Id)).ToList();
                    foreach (var item in contact)
                    {
                        item.CustomerTypeId = mdl.CustomerTypeId;
                        item.UpdatedUserId = UserSession.UserId;
                        item.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                        db.Entry(item).State = EntityState.Modified;
                        //db.SaveChanges();
                    }

                    string CurrentTime = Common.GetCurrentDateTime("Time");
                    var OldCallHistory = db.Call_History.Where(s => s.Id == mdl.LastCallHistoryId).FirstOrDefault();
                    if (OldCallHistory != null)
                    {
                        OldCallHistory.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                        OldCallHistory.UpdatedUserId = UserSession.UserId;
                        db.Entry(OldCallHistory).State = EntityState.Modified;
                    }
                    Call_History history = new Call_History();
                    history.CallId = mdl.CallId;
                    if (!string.IsNullOrEmpty(mdl.CallDate))
                    {
                        history.CallDate = Convert.ToDateTime(mdl.CallDate);
                    }
                    else
                    {
                        history.CallDate = Convert.ToDateTime(DateAndTime);

                    }
                    if (!String.IsNullOrEmpty(mdl.NextCallDate))
                    {
                        string Nextdate = mdl.NextCallDate + " " + CurrentTime;
                        history.NextCallDate = Convert.ToDateTime(Nextdate);//, System.Globalization.CultureInfo.InvariantCulture
                    }
                    history.MeetingTypeId = mdl.MeetingTypeId;
                    history.CallTypeId = mdl.CallTypeId;
                    history.IsCallNotReceived = mdl.IsCallNotReceived;
                    history.CallRemark = mdl.CallRemark;
                    history.IsActive = true;
                    history.IsDeleted = false;
                    history.CreatedUserId = UserSession.UserId;
                    history.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Call_History.Add(history);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = mdl.CallId;
                        response.Message = "Updated Successfully";
                        mdl.CallId = mdl.CallId;

                        mdl.LastCallHistoryId = history.Id;
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = mdl.CallId;
                        response.Message = "Rows not affected.";
                        //mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        //mdl.UpdatedOn = "";
                        mdl.CallId = mdl.CallId;
                        mdl.FirstCallProductId = 0;
                        mdl.LastCallHistoryId = 0;
                        response.Data = mdl;
                    }

                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update1(SP_Select_Industry mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var ind = db.IndustryType_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    ind.IndustryType = mdl.IndustryType;
                    ind.IsActive = mdl.IsActive;
                    string DateAndTime = Common.GetCurrentDateTime();
                    ind.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    ind.UpdatedUserId = UserSession.UserId;
                    db.Entry(ind).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = ind.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = ind.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = ind.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.Call_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }

        public static Task<Response_mdl> AddCallHistory(SP_Select_Call mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    string DateAndTime = Common.GetCurrentDateTime();
                    string CurrentTime = Common.GetCurrentDateTime("Time");
                    Call_History history = new Call_History();
                    history.CallId = mdl.CallId;
                    if (String.IsNullOrEmpty(mdl.CallDate))
                    {
                        history.CallDate = Convert.ToDateTime(DateAndTime);
                    }
                    else
                    {
                        history.CallDate = Convert.ToDateTime(mdl.CallDate);//, System.Globalization.CultureInfo.InvariantCulture
                    }
                    if (!String.IsNullOrEmpty(mdl.NextCallDate))
                    {
                        string Nextdate = mdl.NextCallDate + " " + CurrentTime;
                        history.NextCallDate = Convert.ToDateTime(Nextdate);//, System.Globalization.CultureInfo.InvariantCulture
                    }
                    history.IsActive = true;
                    history.IsDeleted = false;
                    history.IsCallNotReceived = mdl.IsCallNotReceived;
                    history.CreatedUserId = UserSession.UserId;
                    history.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Call_History.Add(history);
                    if (db.SaveChanges() > 0)
                    {
                        response.IsSuccess = true;
                        response.Id = history.Id;
                        response.Message = "Added Successfully";
                        mdl.CallId = (long)history.CallId;
                        mdl.LastCallHistoryId = history.Id;
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = history.Id;
                        response.Message = "Rows not affected.";
                        //mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        //mdl.UpdatedOn = "";
                        mdl.CallId = (long)history.CallId;
                        mdl.FirstCallProductId = 0;
                        mdl.LastCallHistoryId = 0;
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region ---Verification---
        public static bool IsCallExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.Call_Master.Any(e => e.Id == id);
        }
        #endregion
    }
}
