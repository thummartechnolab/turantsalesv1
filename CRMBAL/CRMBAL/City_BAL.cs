﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
namespace CRMBAL
{
    public class City_BAL
    {
        #region ---Grid Selection---
        public static Task<SP_Select_City> GetFilter(NameValueCollection filter)
        {
            SP_Select_City City = new SP_Select_City()
            {

                CityName = filter["CityName"]
            };
            return Task.FromResult<SP_Select_City>(City);
        }
        public static List<SP_Select_City> GetCityJsGridList(NameValueCollection objfilter, long UserId, string UserTypeCode = "AA")
        {
            List<SP_Select_City> select_city = new List<SP_Select_City>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "City");
                var A = new SqlParameter("@A", "CityList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_city = ctx.Database.SqlQuery<SP_Select_City>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                select_city = select_city.Where(c => c.UserId == UserId && (String.IsNullOrEmpty(filter.CityName) || c.CityName.ToLower().Contains(filter.CityName.ToLower()))).ToList();
            }

            return select_city;

        }
        public static List<SP_AutoComplete> GetCityAutoComplete(long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            autoCompletes = Common_BAL.GetAutoComplete("Id", "CityName", "City_Master", UserId, SearchText);
            return autoCompletes;
        }
        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_City mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var cityMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "City", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    City_Master _Master = new City_Master();
                    _Master = (City_Master)cityMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.UserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.City_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_City mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var city = db.City_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    city.CityName = mdl.CityName;
                    city.IsActive = mdl.IsActive;
                    string DateAndTime = Common.GetCurrentDateTime();
                    city.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    city.UpdatedUserId = UserSession.UserId;
                    db.Entry(city).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = city.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = city.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = city.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.City_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region ---Verification---
        public static City_Master GetCityDetail(string CityName)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            return db.City_Master.Where(s => s.CityName.ToLower() == CityName.ToLower()).FirstOrDefault();
        }
        public static bool IsCityAleradyExists(string CityName, long id = 0,long UserId=0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.City_Master.Any(s => s.CityName.ToLower() == CityName.ToLower() && s.Id != id);
                }
                else if (UserId > 0) {
                    IsSuccess = db.City_Master.Any(s => s.CityName.ToLower() == CityName.ToLower() && s.UserId == UserId);
                }
                else
                {
                    IsSuccess = db.City_Master.Any(s => s.CityName.ToLower() == CityName.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }
        public static bool IsCityExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.City_Master.Any(e => e.Id == id);
        }
        #endregion


        public static City_Master Fetch_City_Master_by_ID(int id)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            City_Master CityMaster = new City_Master();
            CityMaster = db.City_Master.Where(s => s.Id == id).FirstOrDefault();
            return CityMaster;
        }
    }
}
