﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMUtility;
namespace CRMBAL
{
    public class Common_BAL
    {
        public static Dictionary<string, object> GetUserPackageRestriction()
        {
            Dictionary<string, object> valuePairs = new Dictionary<string, object>();
            List<SP_Select_UserPackageSubscription> lst = new List<SP_Select_UserPackageSubscription>();
            lst = User_BAL.GetUserSubscription(UserSession.UserId);
            List<string> keys = new List<string>();
            keys = lst.Select(s => s.RestrictionKey).Distinct().ToList();
            foreach (var key in keys)
            {
                var keyValue = lst.Where(s => s.RestrictionKey == key).ToList();
                valuePairs.Add(key, keyValue);
            }
            return valuePairs;
        }
        public static SP_Select_UserPackageSubscription VerifyUserAllowToPerformAction(string ActionType, long RefrenceId, ref bool IsAllowed)
        {
            SP_Select_UserPackageSubscription response = new SP_Select_UserPackageSubscription();
            List<SP_Select_UserPackageSubscription> lst = new List<SP_Select_UserPackageSubscription>();
            lst = User_BAL.GetUserSubscription(UserSession.UserId);
            response = lst.Where(s => s.RestrictionKey == ActionType.ToString() && s.RestrictionRowId == RefrenceId).FirstOrDefault();
            if (response != null)
            {
                IsAllowed = response.IsAllowed;
            }
            return response;
        }
        #region ---FillDropdown---
        public static List<SelectListItem> RatingDropDown()
        {
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            for (int i = 10; i >= 1; i--)
            {
                var selectItem = new SelectListItem() { Text = i.ToString(), Value = i.ToString() };
                selectListItems.Add(selectItem);
            }
            return selectListItems;
        }
        public static List<SelectListItem> GenderDropdown()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Male", Value = "Male" });
            items.Add(new SelectListItem() { Text = "Female", Value = "Female" });
            return items;
        }
        #endregion
        #region ---AutoComplete--
        public static List<SP_AutoComplete> GetAutoComplete(string ValueField, string TextField, string TableName, long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var User = new SqlParameter("@ModuleName", "AutoComplete");
                var UserList = new SqlParameter("@A", ValueField);
                var B = new SqlParameter("@B", TextField);
                var C = new SqlParameter("@C", TableName);
                var D = new SqlParameter("@D", UserId.ToString());
                var E = new SqlParameter("@E", SearchText);
                autoCompletes = ctx.Database.SqlQuery<SP_AutoComplete>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C,@D,@E", User, UserList, B, C, D, E).ToList();
            }

            return autoCompletes;

        }
        #endregion
    }
}
