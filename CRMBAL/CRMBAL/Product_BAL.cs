﻿using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMBAL
{
    public class Product_BAL
    {
        #region ---Grid Selection---
        public static Task<SP_Select_Product> GetFilter(NameValueCollection filter)
        {
            SP_Select_Product product = new SP_Select_Product()
            {

                ProductName = filter["ProductName"]
            };
            return Task.FromResult<SP_Select_Product>(product);
        }
        public static List<SP_Select_Product> GetProductJsGridList(NameValueCollection objfilter, long UserId, string UserTypeCode = "AA")
        {
            List<SP_Select_Product> select_Products = new List<SP_Select_Product>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Product");
                var A = new SqlParameter("@A", "ProductList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_Products = ctx.Database.SqlQuery<SP_Select_Product>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                select_Products = select_Products.Where(c => c.UserId == UserId && (String.IsNullOrEmpty(filter.ProductName) || c.ProductName.ToLower().Contains(filter.ProductName.ToLower()))).ToList();
            }

            return select_Products;

        }
        #endregion
        #region ---Select Method---
        public static List<SP_AutoComplete> GetProductAutoComplete(long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            autoCompletes = Common_BAL.GetAutoComplete("Id", "ProductName", "Product_Master", UserId, SearchText);
            return autoCompletes;
        }
        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_Product mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var prdMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "Product", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    Product_Master _Master = new Product_Master();
                    _Master = (Product_Master)prdMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.UserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Product_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Product mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var product = db.Product_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    product.ProductName = mdl.ProductName;
                    product.IsActive = mdl.IsActive;
                    string DateAndTime = Common.GetCurrentDateTime();
                    product.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    product.UpdatedUserId = UserSession.UserId;
                    db.Entry(product).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = product.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = product.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = product.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var product = db.Product_Master.Where(s => s.Id == id).FirstOrDefault();
                    product.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    product.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    product.UpdatedUserId = UserSession.UserId;
                    db.Entry(product).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = product.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region --Insert Update Operation For Contact Product--
        public static Task<Response_mdl> AddContactProduct(SP_Select_ContactProduct mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    string DateAndTime = Common.GetCurrentDateTime();
                    ContactProduct_Master _Master = new ContactProduct_Master();
                    _Master.ProductId = mdl.ProductId;
                    _Master.CallId = mdl.CallId;
                    _Master.ConatctId = mdl.ConatctId;
                    _Master.Unit = mdl.Unit;
                    _Master.Value = mdl.Value;
                    _Master.Rating = mdl.Rating;
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    _Master.IsActive = true;
                    _Master.IsDeleted = false;
                    db.ContactProduct_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;

                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";

                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> UpdateContactProduct(SP_Select_ContactProduct mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var product = db.ContactProduct_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    if (product != null)
                    {
                        product.ProductId = mdl.ProductId;
                        product.Unit = mdl.Unit;
                        product.Value = mdl.Value;
                        product.Rating = mdl.Rating;
                        string DateAndTime = Common.GetCurrentDateTime();
                        product.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                        product.UpdatedUserId = UserSession.UserId;
                        db.Entry(product).State = EntityState.Modified;
                        if (db.SaveChanges() > 0)
                        {
                            response.Id = product.Id;
                            response.IsSuccess = true;

                            response.Message = "Updated Successfully";

                            response.Data = mdl;

                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Id = product.Id;
                            response.Message = "Rows not Updated.";
                            mdl.Id = product.Id;
                            response.Data = mdl;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region ---Verification---
        public static Product_Master GetProductDetail(string ProductName)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            return db.Product_Master.Where(s => s.ProductName.ToLower() == ProductName.ToLower()).FirstOrDefault();
        }
        public static bool IsProductAleradyExists(string ProductName, long id = 0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.Product_Master.Any(s => s.ProductName.ToLower() == ProductName.ToLower() && s.Id != id);
                }
                else
                {
                    IsSuccess = db.Product_Master.Any(s => s.ProductName.ToLower() == ProductName.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }
        public static bool IsProductExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.Product_Master.Any(e => e.Id == id);
        }
        public static Task<Response_mdl> VerifyMaster(string Type, SP_Select_ContactProduct mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                long UserId = UserSession.UserId;
                if (mdl.ProductId == 0)
                {
                    var Product = Product_BAL.GetProductDetail(mdl.ProductName.Trim());
                    if (Product == null)
                    {
                        SP_Select_Product _Product = new SP_Select_Product() { ProductName = mdl.ProductName.Trim(), IsActive = true };
                        var prdResponse = Product_BAL.Add(_Product).Result;
                        if (prdResponse.IsSuccess)
                        {
                            mdl.ProductId = prdResponse.Id;
                        }
                    }
                    else
                    {
                        mdl.ProductId = Product.Id;
                    }
                }
                if (Type == "Add")
                {
                    response = Product_BAL.AddContactProduct(mdl).Result;
                }
                else if (Type == "Modify")
                {
                    response = Product_BAL.UpdateContactProduct(mdl).Result;
                }
            }
            catch (Exception ex)
            {


            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
    }
}
