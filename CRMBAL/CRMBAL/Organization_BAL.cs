﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
namespace CRMBAL
{
    public class Organization_BAL
    {
        #region Organization Grid
        public static Task<SP_Select_Organization> GetFilter(NameValueCollection filter)
        {
            SP_Select_Organization _Organization = new SP_Select_Organization()
            {

                OrganisationName = filter["OrganisationName"]
            };
            return Task.FromResult<SP_Select_Organization>(_Organization);
        }
        public static List<SP_Select_Organization> GetorganizationJsGridList(NameValueCollection objfilter, long UserId, string UserTypeCode = "AA")
        {
            List<SP_Select_Organization> select_Industries = new List<SP_Select_Organization>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Organization");
                var A = new SqlParameter("@A", "OrganizationList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_Industries = ctx.Database.SqlQuery<SP_Select_Organization>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                select_Industries = select_Industries.Where(c => c.UserId == UserId && (String.IsNullOrEmpty(filter.OrganisationName) || c.OrganisationName.ToLower().Contains(filter.OrganisationName.ToLower()))).ToList();
            }

            return select_Industries;

        }
        #endregion
        #region ---Select Method---
        public static List<SP_AutoComplete> GetOrganisationAutoComplete(long UserId,string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            autoCompletes = Common_BAL.GetAutoComplete("Id", "OrganisationName", "Organisation_Master", UserId, SearchText);
            return autoCompletes;
        }
        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_Organization mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var OrgMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "Organization", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    Organisation_Master _Master = new Organisation_Master();
                    _Master = (Organisation_Master)OrgMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.UserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Organisation_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Organization mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var org = db.Organisation_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    //org.IndustryTypeId = mdl.IndustryType;
                    org.OrganisationName = mdl.OrganisationName;
                    org.IsActive = mdl.IsActive;
                    string DateAndTime = Common.GetCurrentDateTime();
                    org.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    org.UpdatedUserId = UserSession.UserId;
                    db.Entry(org).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = org.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = org.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = org.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.Organisation_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region ---Verification---
        public static Organisation_Master GetOrganisationDetail(string OrganisationName)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            return db.Organisation_Master.Where(s => s.OrganisationName.ToLower() == OrganisationName.ToLower()).FirstOrDefault();
        }
        public static bool IsOrganisationAleradyExists(string OrganisationName, long id = 0,long UserId=0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.Organisation_Master.Any(s => s.OrganisationName.ToLower() == OrganisationName.ToLower() && s.Id != id);
                }
                else if (UserId > 0)
                {
                    IsSuccess = db.Organisation_Master.Any(s => s.OrganisationName.ToLower() == OrganisationName.ToLower() && s.UserId == UserId);
                }
                else
                {
                    IsSuccess = db.Organisation_Master.Any(s => s.OrganisationName.ToLower() == OrganisationName.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }
        public static bool IsOrganizationExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.Organisation_Master.Any(e => e.Id == id);
        }
        #endregion

        public static Organisation_Master Fetch_Organisation_Master_by_ID(int id)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            Organisation_Master OrganisationMaster = new Organisation_Master();
            OrganisationMaster = db.Organisation_Master.Where(s => s.Id == id).FirstOrDefault();
            return OrganisationMaster;
        }

    }
}
