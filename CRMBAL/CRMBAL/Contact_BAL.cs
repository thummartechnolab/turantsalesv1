﻿using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMBAL
{
    public class Contact_BAL
    {
        #region ---Select Method---
        public static List<SP_AutoComplete> GetConatctAutoComplete(long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            //"INSERT INTO Product (UserId, Name) '" +"'VALUES (null, '" + objProduct.Name + "');",
            string Quote = "'";
            string BlankSpac = " ";
            string Q = "'CASE When(LastName IS NULL) then FirstName Else FirstName+ " + Quote+Quote + BlankSpac + Quote +Quote+ "+LastName END'";
            //  autoCompletes = Common_BAL.GetAutoComplete("Id", Q, "Contact_Master", UserId, SearchText);
            //autoCompletes = Common_BAL.GetAutoComplete("Id", "CONCAT(FirstName,'' '',LastName)", "Contact_Master", UserId, SearchText);
            autoCompletes = Common_BAL.GetAutoComplete("Id", "FirstName", "Contact_Master", UserId, SearchText);
            return autoCompletes;
        }
        public static List<SP_AutoComplete> GetLastNameAutoComplete(long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            autoCompletes = Common_BAL.GetAutoComplete("Id", "LastName", "Contact_Master", UserId, SearchText);
            return autoCompletes;
        }
        public static List<SP_AutoComplete> GetCityAutoComplete(long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            autoCompletes = Common_BAL.GetAutoComplete("Id", "CityName", "City_Master", UserId, SearchText);
            return autoCompletes;
        }
        public static List<CustomerType_Master> GetCustomerTypes()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<CustomerType_Master> customerTypes = new List<CustomerType_Master>();
            customerTypes = db.CustomerType_Master.Where(s => s.IsActive == true && s.IsDeleted == false).ToList();
            return customerTypes;
        }
        public static List<SP_Select_Contact> GetContacts(long FromContactId,long UserId,long ContactId)
        {
            List<SP_Select_Contact> select_Contacts = new List<SP_Select_Contact>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Contact");
                var A = new SqlParameter("@A", "GetContact");
                var B = new SqlParameter("@B", FromContactId.ToString());
                var C = new SqlParameter("@C", (UserId > 0 ? UserId.ToString() :"0"));
                var D = new SqlParameter("@D", (ContactId > 0 ? ContactId.ToString() : "0"));
                select_Contacts = ctx.Database.SqlQuery<SP_Select_Contact>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C,@D", Module, A, B, C,D).ToList();

            }
            return select_Contacts;

        }
        public static List<SP_Select_Contact> GetContactList(long FromContactId, long UserId, long ContactId)
        {
            List<SP_Select_Contact> select_Contacts = new List<SP_Select_Contact>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Contact");
                var A = new SqlParameter("@A", "GetContactList");
                var B = new SqlParameter("@B", FromContactId.ToString());
                var C = new SqlParameter("@C", (UserId > 0 ? UserId.ToString() : "0"));
                var D = new SqlParameter("@D", (ContactId > 0 ? ContactId.ToString() : "0"));
                select_Contacts = ctx.Database.SqlQuery<SP_Select_Contact>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C,@D", Module, A, B, C, D).ToList();

            }
            return select_Contacts;

        }
        public static List<SP_Select_ContactProduct> GetContactProduct(long ContactId, long Id=0)
        {
            List<SP_Select_ContactProduct> select_ContactProducts = new List<SP_Select_ContactProduct>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Contact");
                var A = new SqlParameter("@A", "GetContactProduct");
                var B = new SqlParameter("@B", (Id > 0 ? Id.ToString() : "0"));
                var C = new SqlParameter("@C", (ContactId > 0 ? ContactId.ToString() : "0"));
                select_ContactProducts = ctx.Database.SqlQuery<SP_Select_ContactProduct>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C", Module, A, B,C).ToList();

            }
            return select_ContactProducts;

        }
        #endregion
        #region ---Grid Selection---
        public static Task<SP_Select_Contact> GetFilter(NameValueCollection filter)
        {
            SP_Select_Contact call = new SP_Select_Contact()
            {
                OrganisationName = filter["OrganisationName"],
                FirstName = filter["FirstName"],
                LastName = filter["LastName"],
                PrimaryPhoneNo = filter["PrimaryPhoneNo"],
                PhoneNo2 = filter["PhoneNo2"],
                Designation = filter["Designation"],
                CityName = filter["CityName"],
                CustomerType = filter["CustomerType"],
                Extn = filter["Extn"],
            };
            return Task.FromResult<SP_Select_Contact>(call);
        }
        public static List<SP_Select_Contact> GetContactJsGridList(NameValueCollection objfilter, long UserId, long ContactId, string UserTypeCode = "AA")
        {
            List<SP_Select_Contact> select_Calls = new List<SP_Select_Contact>();
            var filter = GetFilter(objfilter).Result;
            select_Calls = GetContactList(0, UserId, ContactId);
            select_Calls = select_Calls.Where(c => (String.IsNullOrEmpty(filter.OrganisationName) || c.OrganisationName.Contains(filter.OrganisationName))
            && (String.IsNullOrEmpty(filter.FirstName) || c.FirstName.Contains(filter.FirstName))
            && (String.IsNullOrEmpty(filter.LastName) || c.LastName.Contains(filter.LastName))
            && (String.IsNullOrEmpty(filter.PrimaryPhoneNo) || c.PrimaryPhoneNo.Contains(filter.PrimaryPhoneNo))
            && (String.IsNullOrEmpty(filter.PhoneNo2) || c.PhoneNo2.Contains(filter.PhoneNo2))
            && (String.IsNullOrEmpty(filter.Designation) || c.Designation.Contains(filter.Designation))
            && (String.IsNullOrEmpty(filter.CityName) || c.CityName.Contains(filter.CityName))
            && (String.IsNullOrEmpty(filter.CustomerType) || c.CustomerTypeCode.Contains(filter.CustomerType))
            && (String.IsNullOrEmpty(filter.Extn) || c.Extn.Contains(filter.Extn))).ToList();
            return select_Calls;

        }
        #endregion
        #region --- Insert Update Operation ---
        public static Task<Response_mdl> AddContact(SP_Select_Contact mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    Contact_Master _Master = new Contact_Master();
                    _Master.OrganisationId = mdl.OrganisationId;
                    _Master.DesignationId = mdl.DesignationId;
                    _Master.FirstName = mdl.FirstName;
                    _Master.LastName = mdl.LastName;

                    _Master.CityId = mdl.CityId;
                    _Master.PrimaryPhoneNo = mdl.PrimaryPhoneNo;
                    _Master.PhoneNo2 = mdl.PhoneNo2;
                    _Master.Extn = mdl.Extn;
                    _Master.UnitNo = mdl.UnitNo;
                    _Master.Street = mdl.Street;
                    _Master.PinCode = mdl.PinCode;
                    _Master.PrimaryEmail = mdl.PrimaryEmail;
                    _Master.ScondaryEmail = mdl.ScondaryEmail;
                    _Master.Chat1 = mdl.Chat1;
                    _Master.Chat2 = mdl.Chat2;
                    _Master.ParentContactId = mdl.ParentContactId;
                    if (!String.IsNullOrEmpty(mdl.Extn))
                    {
                        _Master.Extn = mdl.Extn;
                    }
                    _Master.CustomerTypeId = 0;
                    _Master.UserId = UserSession.UserId;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    _Master.CreadtedUserId = _Master.UserId;
                    _Master.IsActive = true;
                    _Master.IsDeleted = false;
                    db.Contact_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";


                        response.Data = mdl;
                        // response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";

                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> UpdateContact(SP_Select_Contact mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    Contact_Master _Master = db.Contact_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    if (_Master != null)
                    {
                        _Master.OrganisationId = mdl.OrganisationId;
                        _Master.DesignationId = mdl.DesignationId;
                        _Master.FirstName = mdl.FirstName;
                        _Master.LastName = mdl.LastName;

                        _Master.CityId = mdl.CityId;
                        _Master.PrimaryPhoneNo = mdl.PrimaryPhoneNo;
                        _Master.PhoneNo2 = mdl.PhoneNo2;

                        _Master.UnitNo = mdl.UnitNo;
                        _Master.Street = mdl.Street;
                        _Master.PinCode = mdl.PinCode;
                        _Master.PrimaryEmail = mdl.PrimaryEmail;
                        _Master.ScondaryEmail = mdl.ScondaryEmail;
                        _Master.Chat1 = mdl.Chat1;
                        _Master.Chat2 = mdl.Chat2;
                        if (!String.IsNullOrEmpty(mdl.Extn))
                        {
                            _Master.Extn = mdl.Extn;
                        }

                        string DateAndTime = Common.GetCurrentDateTime();
                        _Master.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                        _Master.UpdatedUserId = UserSession.UserId;
                        db.Entry(_Master).State = EntityState.Modified;
                        if (db.SaveChanges() > 0)
                        {

                            response.IsSuccess = true;
                            response.Id = _Master.Id;
                            response.Message = "Updated Successfully";


                            response.Data = mdl;
                            // response.Data = mdl;
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Id = _Master.Id;
                            response.Message = "Rows not affected.";

                        }
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Add(SP_Select_Call mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    Contact_Master _Master = new Contact_Master();
                    _Master.FirstName = mdl.FirstName;
                    _Master.LastName = mdl.LastName;
                    _Master.OrganisationId = mdl.OrganisationId;
                    _Master.DesignationId = mdl.DesignationId;
                    _Master.CityId = mdl.CityId;
                    _Master.PrimaryPhoneNo = mdl.PrimaryPhoneNo;
                    _Master.ParentContactId = 0;
                    _Master.IndustryTypeId = mdl.IndustryTypeId;
                    if (!String.IsNullOrEmpty(mdl.Extn))
                    {
                        _Master.Extn = mdl.Extn;
                    }
                    _Master.UserId = UserSession.UserId;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    _Master.CreadtedUserId = _Master.UserId;
                    _Master.IsActive = true;
                    _Master.IsDeleted = false;
                    db.Contact_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.CallId = _Master.Id;

                        response.Data = mdl;
                        // response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";

                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Call mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    Contact_Master _Master = db.Contact_Master.Where(s => s.Id == mdl.ContactId).FirstOrDefault();
                    if (_Master != null)
                    {
                        _Master.FirstName = mdl.FirstName;
                        _Master.LastName = mdl.LastName;
                        _Master.OrganisationId = mdl.OrganisationId;
                        _Master.DesignationId = mdl.DesignationId;
                        if (_Master.CustomerTypeId == null)
                        {
                            _Master.CustomerTypeId = mdl.CustomerTypeId;
                        }
                        _Master.CityId = mdl.CityId;
                        _Master.PrimaryPhoneNo = mdl.PrimaryPhoneNo;
                        if(!String.IsNullOrEmpty(mdl.Extn))
                        {
                            _Master.Extn = mdl.Extn;
                        }
                        if (_Master.IndustryTypeId == null || _Master.IndustryTypeId==0)
                        {
                            _Master.IndustryTypeId = mdl.IndustryTypeId;
                        }
                        _Master.UserId = UserSession.UserId;
                        string DateAndTime = Common.GetCurrentDateTime();
                        _Master.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                        _Master.UpdatedUserId = _Master.UserId;
                        _Master.IsActive = true;
                        _Master.IsDeleted = false;
                        db.Entry(_Master).State = EntityState.Modified;
                        if (db.SaveChanges() > 0)
                        {

                            response.IsSuccess = true;
                            response.Id = _Master.Id;
                            response.Message = "Updated Successfully";


                            response.Data = mdl;
                            // response.Data = mdl;
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Id = _Master.Id;
                            response.Message = "Rows not affected.";

                        }
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.Contact_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region ---Verification---
        public static Contact_Master GetContact(string Phone1)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            return db.Contact_Master.Where(s => s.PrimaryPhoneNo.ToLower() == Phone1.ToLower()).FirstOrDefault();
        }
        public static CustomerType_Master GetCustomerTypeByTypeCode(string CustomerTypeCode)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            return db.CustomerType_Master.Where(s => s.CustomerTypeCode.ToLower() == CustomerTypeCode.ToLower()).FirstOrDefault();
        }
        public static Task<Response_mdl> VerifyMaster(string Type, SP_Select_Contact mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                long UserId = UserSession.UserId;
                if (!String.IsNullOrEmpty(mdl.FirstName) && !string.IsNullOrEmpty(mdl.LastName))
                {
                    if (mdl.OrganisationId == 0)
                    {
                        if (!String.IsNullOrEmpty(mdl.OrganisationName.Trim()))
                        {
                            var Organization = Organization_BAL.GetOrganisationDetail(mdl.OrganisationName.Trim());
                            if (Organization == null)
                            {
                                SP_Select_Organization org = new SP_Select_Organization() { OrganisationName = mdl.OrganisationName.Trim(), IsActive = true };
                                var orgResponse = Organization_BAL.Add(org).Result;
                                if (orgResponse.IsSuccess)
                                {
                                    mdl.OrganisationId = orgResponse.Id;
                                }
                            }
                            else
                            {
                                mdl.OrganisationId = Organization.Id;
                            }
                        }
                        else
                        {
                            mdl.OrganisationId = 0;
                        }
                    }

                    if (mdl.DesignationId == 0)
                    {
                        if (!String.IsNullOrEmpty(mdl.Designation))
                        {
                            var Designation = Designation_BAL.GetDesignationDetail(mdl.Designation.Trim());
                            if (Designation == null)
                            {
                                SP_Select_Designation _Designation = new SP_Select_Designation() { Designation = mdl.Designation.Trim(), IsActive = true };
                                var dsgResponse = Designation_BAL.Add(_Designation).Result;
                                if (dsgResponse.IsSuccess)
                                {
                                    mdl.DesignationId = dsgResponse.Id;
                                }
                            }
                            else
                            {
                                mdl.DesignationId = Designation.Id;
                            }
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.IsErrorOccur = true;
                            response.Message = "Designation Required";
                        }
                    }

                    if (mdl.CityId == 0)
                    {
                        if (!String.IsNullOrEmpty(mdl.CityName.Trim()))
                        {
                            var City = City_BAL.GetCityDetail(mdl.CityName.Trim());
                            if (City == null)
                            {
                                SP_Select_City _City = new SP_Select_City() { CityName = mdl.CityName.Trim(), IsActive = true };
                                var ctyResponse = City_BAL.Add(_City).Result;
                                if (ctyResponse != null)
                                {
                                    mdl.CityId = ctyResponse.Id;
                                }
                            }
                            else
                            {
                                mdl.CityId = City.Id;
                            }
                        }
                        else
                        {
                            mdl.CityId = 0;
                        }
                    }
                    if (Type == "Add")
                    {
                        response = Contact_BAL.AddContact(mdl).Result;
                    }
                    else if (Type == "Modify")
                    {
                        response = Contact_BAL.UpdateContact(mdl).Result;
                    }
                }
                else {
                    response.IsSuccess = false;
                    response.IsErrorOccur = true;
                    response.Message = "First Name & Last Name Required";
                }
            }
            catch (Exception ex)
            {


            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static bool IsContactExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.Contact_Master.Any(e => e.Id == id);
        }

        #endregion
        public static List<Contact_Master> GetContact_Master()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<Contact_Master> ContactMaster = new List<Contact_Master>();
            long UserId = UserSession.UserId;
            ContactMaster = db.Contact_Master.Where(s => s.IsActive == true && s.IsDeleted == false && s.UserId==UserId).ToList();
            return ContactMaster;
        }
    }
}
