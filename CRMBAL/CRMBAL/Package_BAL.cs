﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;

namespace CRMBAL
{
    public class Package_BAL
    {
        #region --Selection--
        public static List<Package_Master> GetPackages()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<Package_Master> packages = new List<Package_Master>();
            packages = db.Package_Master.Where(s => s.IsActive == true && s.IsDeleted == false).ToList();
            return packages;
        }
        public static Package_Master GetPackageById(int id)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            Package_Master packages = new Package_Master();
            packages = db.Package_Master.Where(s => s.Id == id).FirstOrDefault();
            return packages;
        }
        public static Tax_Master GetTaxById(long id)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            Tax_Master tax = new Tax_Master();
            tax = db.Tax_Master.Where(s => s.Id == id).FirstOrDefault();
            return tax;
        }
        public static List<SP_Select_Package> GetPackageByID(long Id)
        {
            List<SP_Select_Package> select_Package = new List<SP_Select_Package>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Package");
                var A = new SqlParameter("@A", "PackageById");
                var B = new SqlParameter("@B", Id);
                select_Package = ctx.Database.SqlQuery<SP_Select_Package>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
            }

            return select_Package;
        }
        public static List<PackageGroup_Master> GetPackageGroup()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<PackageGroup_Master> packages_group = new List<PackageGroup_Master>();
            packages_group = db.PackageGroup_Master.Where(s => s.IsActive == true && s.IsDeleted == false).ToList();
            return packages_group;
        }

        #endregion

        #region ---Grid Selection---
        public static Task<SP_Select_Package> GetFilter(NameValueCollection filter)
        {
            SP_Select_Package Package = new SP_Select_Package()
            {

                PackageName = filter["PackageName"],
                PackageGroupName=filter["PackageGroupName"],
                ValidityInDays=Convert.ToInt32(filter["ValidityInDays"]),
                ValidityMonths= Convert.ToInt32(filter["ValidityMonths"]),
                Price=Convert.ToDecimal(filter["Price"])
            };
            return Task.FromResult<SP_Select_Package>(Package);
        }
        public static List<SP_Select_Package> GetPackageJsGridList(NameValueCollection objfilter, long UserId, string UserTypeCode = "AA")
        {
            List<SP_Select_Package> select_PackageList = new List<SP_Select_Package>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Package");
                var A = new SqlParameter("@A", "PackageList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_PackageList = ctx.Database.SqlQuery<SP_Select_Package>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                select_PackageList = select_PackageList.Where(c => (String.IsNullOrEmpty(filter.PackageName) || c.PackageName.ToLower().Contains(filter.PackageName.ToLower())) && (String.IsNullOrEmpty(filter.PackageGroupName) || c.PackageGroupName.ToLower().Contains(filter.PackageGroupName.ToLower()))).ToList();

            }

            return select_PackageList;

        }
        public static List<SP_Select_PackageCustom> GetPackageList()
        {
            List<SP_Select_PackageCustom> select_PackageList = new List<SP_Select_PackageCustom>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Package");
                var A = new SqlParameter("@A", "PackageList");
                select_PackageList = ctx.Database.SqlQuery<SP_Select_PackageCustom>("exec SP_SELECT_DATA @ModuleName,@A", Module, A).ToList();
            }
            return select_PackageList;
        }
        public static List<SP_Select_PackageCustom> GetPlanList()
        {
            List<SP_Select_PackageCustom> select_PackageList = new List<SP_Select_PackageCustom>();
            select_PackageList = GetPackageList().Where(m=>m.IsDisplay==true).ToList();
            if (select_PackageList != null)
            {
                if (select_PackageList.Count() > 0)
                {
                    foreach(var item in select_PackageList)
                    {
                        List<SP_Select_PackageGroupConfiguration> groupConfigurations = new List<SP_Select_PackageGroupConfiguration>();
                        groupConfigurations = PackageGroup_BAL.GetPackageGroupConfigurations(0, item.PackageGroupId);
                        item.PackageGroupConfigurations = groupConfigurations;
                    }
                }
            }
            return select_PackageList;
        }
        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_Package mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var pacMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "Package", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    Package_Master _Master = new Package_Master();
                    _Master = (Package_Master)pacMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Package_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Package mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var pac = db.Package_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    pac.PackageName = mdl.PackageName;
                    pac.PackageGroupId = mdl.PackageGroupId;
                    pac.ValidityInDays = mdl.ValidityInDays;
                    pac.ValidityMonths = mdl.ValidityMonths;
                    pac.CurrencyCode = mdl.CurrencyCode;
                    pac.Price = mdl.Price;
                    pac.IsAnyRestriction = mdl.IsAnyRestriction;
                    pac.TaxId = mdl.TaxId;
                    pac.IsActive = mdl.IsActive;
                    pac.IsDisplay = mdl.IsDisplay;
                    string DateAndTime = Common.GetCurrentDateTime();
                    pac.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    pac.UpdatedUserId = UserSession.UserId;
                    db.Entry(pac).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = pac.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = pac.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = pac.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.Package_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.IsSuccess = true;
                        response.Message = "Deleted Successfully";

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region Validation
        public static bool IsPackageAleradyExists(string PackageName, long id = 0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.Package_Master.Any(s => s.PackageName.ToLower() == PackageName.ToLower() && s.Id != id);
                }
                else
                {
                    IsSuccess = db.Package_Master.Any(s => s.PackageName.ToLower() == PackageName.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }
        public static bool IsPackageExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.Package_Master.Any(e => e.Id == id);
        }
        #endregion
    }
}
