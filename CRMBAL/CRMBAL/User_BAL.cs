﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMModel;
using CRMEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System.Collections.Specialized;
using CRMModel.CustomEntity;
using System.Data.SqlClient;
using System.Data.Entity;

namespace CRMBAL
{
    public class User_BAL
    {
        private static List<SP_Select_User> lstUser;
        #region ---Grid Selection---
        public static Task<SP_Select_User> GetFilter(NameValueCollection filter)
        {
            SP_Select_User user = new SP_Select_User()
            {

                FirstName = filter["FirstName"],
                LastName = filter["LastName"],
                ContactNo = filter["ContactNo"],
                Gender = filter["Gender"],
                WorkEmail = filter["WorkEmail"],
                UserType = filter["UserType"],
                PackageName = filter["PackageName"],
                TotalPaidAmount = Convert.ToDecimal(filter["TotalPaidAmount"])

            };
            return Task.FromResult<SP_Select_User>(user);
        }
        public static List<SP_Select_User> GetUserJsGridList(NameValueCollection objfilter, string UserTypeCode = "AA")
        {
            List<SP_Select_User> select_Users = new List<SP_Select_User>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var User = new SqlParameter("@ModuleName", "User");
                var UserList = new SqlParameter("@A", "UserList");
                // List<SP_Select_User> select_Users = new List<SP_Select_User>();

                select_Users = ctx.Database.SqlQuery<SP_Select_User>("exec SP_SELECT_DATA @ModuleName,@A", User, UserList).ToList();
                select_Users = select_Users.Where(c => (String.IsNullOrEmpty(filter.FirstName) || c.FirstName.Contains(filter.FirstName)) &&
                       (String.IsNullOrEmpty(filter.ContactNo) || c.ContactNo.Contains(filter.ContactNo)) && (String.IsNullOrEmpty(filter.LastName) || c.LastName.Contains(filter.LastName))
                       && (String.IsNullOrEmpty(filter.WorkEmail) || c.WorkEmail.Contains(filter.WorkEmail)) && (String.IsNullOrEmpty(filter.Gender) || c.Gender == filter.Gender)
                      && (String.IsNullOrEmpty(filter.UserType) || c.UserType.Contains(filter.UserType)) && (String.IsNullOrEmpty(filter.PackageName) || c.PackageName.Contains(filter.PackageName))).ToList();

            }

            return select_Users;

        }
        public static List<SP_Select_User> GetUserListById(long Id)
        {
            List<SP_Select_User> select_Users = new List<SP_Select_User>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var User = new SqlParameter("@ModuleName", "User");
                var A = new SqlParameter("@A", "UserListById");
                var B = new SqlParameter("@B", Id);
                select_Users = ctx.Database.SqlQuery<SP_Select_User>("exec SP_SELECT_DATA @ModuleName,@A,@B", User, A, B).ToList();

            }

            return select_Users;

        }

        #endregion
        #region ---Selection Method---
        public static Task<SP_Select_User> GetUserByUserName(string UserName)
        {
            Task<SP_Select_User> user = null;
            try
            {
                db_SalesCRMEntities ctx = new db_SalesCRMEntities();

                user = (from s in ctx.User_Master
                        join ut in ctx.UserType_Master on s.UserTypeId equals ut.Id
                        join u in ctx.AspNetUsers on s.UserUniqueId equals u.Id
                        where ((u.UserName.ToLower() == UserName.ToLower()) && (s.IsDeleted == false))
                        select new SP_Select_User
                        {
                            UserId = s.Id,
                            FirstName = s.FirstName,
                            LastName = s.LastName,
                            UserTypeId = (int)s.UserTypeId,
                            UserType = ut.UserType,
                            UserTypeCode = ut.UserTypeCode,
                            IsActive = (bool)s.IsActive,
                            IsApproved = (bool)s.IsApproved,
                            IsDeleted = (bool)s.IsDeleted

                        }).FirstOrDefaultAsync();



            }
            catch (Exception ex)
            {

                throw ex;
            }
            return user;
        }

        public static bool UserExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.User_Master.Any(e => e.Id == id);
        }
        public static User_Master GetUserById(long UserId)
        {
            User_Master user_Master = new User_Master();

            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            user_Master = ctx.User_Master.Where(s => s.Id == UserId).FirstOrDefault();
            return user_Master;

        }

        public static List<SP_Select_UserPackageSubscription> GetUserSubscription(long UserId)
        {
            List<SP_Select_UserPackageSubscription> select_PackageConfig = new List<SP_Select_UserPackageSubscription>();
            var user = User_BAL.GetUserById(UserId);
            if (user != null)
            {
                var userSubscription = user.User_Subscription_Master.Where(s => s.IsActive == true && s.IsDeleted == false).OrderByDescending(s => s.SubscriptionDate).FirstOrDefault();
                if (userSubscription != null)
                {
                    select_PackageConfig = PackageConfiguration_BAL.GetUserPackageRestrictionByUser(UserId, (int)userSubscription.PackageId, userSubscription.Id);
                }
            }
            return select_PackageConfig;
        }
        #endregion
        public static Task<Response_mdl> Add(User_mdl mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    using (var tr = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var PackagDetails = Package_BAL.GetPackageById(mdl.PackageId);
                            var userMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "User", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                            User_Master _Master = new User_Master();
                            _Master = (User_Master)userMst;
                            _Master.IsLicenceExpired = false;
                            string DateAndTime = Common.GetCurrentDateTime();
                            _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                            if (PackagDetails != null)
                            {
                                if (PackagDetails.PackageName.ToLower().Contains("free"))
                                {
                                    _Master.IsApproved = true;
                                }
                            }
                            db.User_Master.Add(_Master);

                            if (PackagDetails != null)
                            {
                                User_Subscription_Master _Subscription_Master = new User_Subscription_Master();
                                _Subscription_Master.PackageId = mdl.PackageId;
                                _Subscription_Master.UserId = _Master.Id;
                                _Subscription_Master.SubscriptionDate = Convert.ToDateTime(DateAndTime);
                                _Subscription_Master.ValidityMonth = PackagDetails.ValidityMonths;
                                _Subscription_Master.ValidityInDays = PackagDetails.ValidityInDays;
                                decimal Total = 0, TaxAmount = 0, TaxPercentage = 0, Price = 0; ;
                                _Subscription_Master.Price = PackagDetails.Price;
                                Price = (decimal)PackagDetails.Price;
                                if ((bool)PackagDetails.IsTaxablePrice)
                                {
                                    var TaxId = (long)PackagDetails.TaxId;
                                    var TaxDetails = Package_BAL.GetTaxById(TaxId);
                                    if (TaxDetails != null)
                                    {
                                        TaxPercentage = (decimal)TaxDetails.TaxPercentage;
                                        _Subscription_Master.TaxPer = TaxDetails.TaxPercentage;
                                        TaxAmount = Price * TaxPercentage / 100;
                                    }

                                }
                                _Subscription_Master.TaxAmount = TaxAmount;
                                Total = Price + TaxAmount;
                                _Subscription_Master.TotalPaidAmount = Total;
                                _Subscription_Master.CurrencyCode = "INR";
                                _Subscription_Master.IsPaid = false;
                                _Subscription_Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                                DateTime SubscriptionExpire = Convert.ToDateTime(DateAndTime).AddDays((double)PackagDetails.ValidityInDays);
                                _Subscription_Master.SubscriptionExpireDate = SubscriptionExpire;
                                _Subscription_Master.IsActive = true;
                                _Subscription_Master.IsDeleted = false;
                                db.User_Subscription_Master.Add(_Subscription_Master);
                                if (db.SaveChanges() > 0)
                                {

                                    response.IsSuccess = true;
                                    response.Id = _Master.Id;
                                    response.Message = "Added Successfully";
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Id = _Master.Id;
                                    response.Message = "Rows not affected.";
                                }
                                tr.Commit();

                            }
                            else
                            {
                                throw new Exception("Package Not Found.");
                            }

                        }
                        catch (Exception ex)
                        {

                            tr.Rollback();
                            throw ex;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_UserDetails mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    using (var tr = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var PackagDetails = Package_BAL.GetPackageById(mdl.PackageId);
                            var user = db.User_Master.Where(s => s.Id == mdl.id).FirstOrDefault();
                            string DateAndTime = Common.GetCurrentDateTime();
                            user.IsLicenceExpired = false;
                            user.IsActive = mdl.Isactive;
                            user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                            db.Entry(user).State = EntityState.Modified;
                            var currentSubscription = db.User_Subscription_Master.Where(s => s.UserId == user.Id && s.IsActive == true && s.IsDeleted == false).OrderByDescending(s => s.CreatedDateTime).FirstOrDefault();

                            if (PackagDetails != null && currentSubscription.PackageId != mdl.PackageId)
                            {
                                //var currentSubscription = db.User_Subscription_Master.Where(s => s.UserId == user.Id && s.IsActive == true && s.IsDeleted == false).OrderByDescending(s => s.CreatedDateTime).FirstOrDefault();
                                currentSubscription.IsActive = false;
                                currentSubscription.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                                currentSubscription.UpdatedUserId = Convert.ToInt32(UserSession.UserId);
                                db.Entry(user).State = EntityState.Modified;
                                db.SaveChanges();

                                User_Subscription_Master _Subscription_Master = new User_Subscription_Master();
                                _Subscription_Master.PackageId = mdl.PackageId;
                                _Subscription_Master.UserId = mdl.id;
                                _Subscription_Master.SubscriptionDate = Convert.ToDateTime(DateAndTime);
                                _Subscription_Master.ValidityMonth = PackagDetails.ValidityMonths;
                                _Subscription_Master.ValidityInDays = PackagDetails.ValidityInDays;
                                decimal Total = 0, TaxAmount = 0, TaxPercentage = 0, Price = 0; ;
                                _Subscription_Master.Price = PackagDetails.Price;
                                Price = (decimal)PackagDetails.Price;
                                if ((bool)PackagDetails.IsTaxablePrice)
                                {
                                    var TaxId = (long)PackagDetails.TaxId;
                                    var TaxDetails = Package_BAL.GetTaxById(TaxId);
                                    if (TaxDetails != null)
                                    {
                                        TaxPercentage = (decimal)TaxDetails.TaxPercentage;
                                        _Subscription_Master.TaxPer = TaxDetails.TaxPercentage;
                                        TaxAmount = Price * TaxPercentage / 100;
                                    }

                                }
                                _Subscription_Master.TaxAmount = TaxAmount;
                                Total = Price + TaxAmount;
                                _Subscription_Master.TotalPaidAmount = Total;
                                _Subscription_Master.CurrencyCode = "INR";
                                _Subscription_Master.IsPaid = Total > 0 ? true : false;
                                _Subscription_Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                                DateTime SubscriptionExpire = Convert.ToDateTime(DateAndTime).AddDays((double)PackagDetails.ValidityInDays);
                                _Subscription_Master.SubscriptionExpireDate = SubscriptionExpire;
                                _Subscription_Master.IsActive = true;
                                _Subscription_Master.IsDeleted = false;
                                db.User_Subscription_Master.Add(_Subscription_Master);
                                if (db.SaveChanges() > 0)
                                {
                                    response.IsSuccess = true;
                                    response.Id = mdl.id;
                                    response.Message = "Added Successfully";
                                }
                                else
                                {
                                    response.IsSuccess = false;
                                    response.Id = mdl.id;
                                    response.Message = "Rows not affected.";
                                }
                               // tr.Commit();

                            }

                            tr.Commit();
                        }
                        catch (Exception ex)
                        {
                            tr.Rollback();
                            throw ex;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(long id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.User_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }

        public static Task<Response_mdl> UpdateLastLogin(long userid)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    using (var tr = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var user = db.User_Master.Where(s => s.Id == userid).FirstOrDefault();
                            string DateAndTime = Common.GetCurrentDateTime();
                            user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);

                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();

                            if (db.SaveChanges() > 0)
                            {
                                response.IsSuccess = true;
                                response.Id = userid;
                                response.Message = "Update Successfully";
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Id = userid;
                                response.Message = "Rows not affected.";
                            }
                            // tr.Commit();
                            tr.Commit();
                        }                            
                        catch (Exception ex)
                        {
                            tr.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }

        public static Task<Response_mdl> CommonAction(int UserId, bool CurrentValue, string ActionType)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.User_Master.Where(s => s.Id == UserId).FirstOrDefault();

                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    bool NewValue = false;
                    if (CurrentValue)
                    {
                        NewValue = false;
                    }
                    else { NewValue = true; }
                    if (ActionType == "IsActive")
                    {
                        user.IsActive = NewValue;
                    }
                    if (ActionType == "IsApproved" || ActionType == "IsPaid")
                    {
                        user.IsApproved = NewValue;
                    }

                    db.Entry(user).State = EntityState.Modified;

                    if (db.SaveChanges() > 0)
                    {
                        if (ActionType == "IsPaid")
                        {
                            var currentSubscription = db.User_Subscription_Master.Where(s => s.UserId == user.Id && s.IsActive == true && s.IsDeleted == false).OrderByDescending(s => s.CreatedDateTime).FirstOrDefault();
                            currentSubscription.IsPaid = true;
                            currentSubscription.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                            currentSubscription.UpdatedUserId = Convert.ToInt32(UserSession.UserId);
                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        response.IsSuccess = true;

                        response.Message = "Modify Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not modified.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }

    }
}
