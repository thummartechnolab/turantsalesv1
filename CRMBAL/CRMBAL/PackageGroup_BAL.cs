﻿using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMBAL
{
    public class PackageGroup_BAL
    {
        #region --Selection--
        public static List<Package_Master> GetPackages()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<Package_Master> packages = new List<Package_Master>();
            packages = db.Package_Master.Where(s => s.IsActive == true && s.IsDeleted == false).ToList();
            return packages;
        }
        public static Package_Master GetPackageById(int id)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            Package_Master packages = new Package_Master();
            packages = db.Package_Master.Where(s => s.Id == id).FirstOrDefault();
            return packages;
        }
        public static Tax_Master GetTaxById(long id)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            Tax_Master tax = new Tax_Master();
            tax = db.Tax_Master.Where(s => s.Id == id).FirstOrDefault();
            return tax;
        }
        public static List<SP_Select_Package> GetPackageByID(long Id)
        {
            List<SP_Select_Package> select_Package = new List<SP_Select_Package>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Package");
                var A = new SqlParameter("@A", "PackageById");
                var B = new SqlParameter("@B", Id);
                select_Package = ctx.Database.SqlQuery<SP_Select_Package>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
            }

            return select_Package;
        }
        public static List<PackageType_Master> GetPackageType()
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            List<PackageType_Master> packageTypes = new List<PackageType_Master>();
            packageTypes = db.PackageType_Master.Where(s => s.IsActive == true && s.IsDeleted == false).ToList();
            return packageTypes;
        }

        #endregion

        #region ---Grid Selection---
        public static Task<SP_Select_PackageGroup> GetFilter(NameValueCollection filter)
        {
            SP_Select_PackageGroup Package = new SP_Select_PackageGroup()
            {
                PackageGroupName = filter["PackageGroupName"],
                PackageType = filter["PackageType"]
            };
            return Task.FromResult<SP_Select_PackageGroup>(Package);
        }
        public static List<SP_Select_PackageGroup> GetPackageGroupJsGridList(NameValueCollection objfilter, long PackageGroupId, string UserTypeCode = "AA")
        {
            List<SP_Select_PackageGroup> select_PackageList = new List<SP_Select_PackageGroup>();
            var filter = GetFilter(objfilter).Result;
            select_PackageList = GetPackageGroups(PackageGroupId);
            select_PackageList = select_PackageList.Where(p => p.PackageGroupName.ToLower().Contains(filter.PackageGroupName.ToLower()) || String.IsNullOrEmpty(filter.PackageGroupName)).ToList();
            return select_PackageList;

        }
        public static List<SP_Select_PackageGroup> GetPackageGroups(long PackageGroupId)
        {
            List<SP_Select_PackageGroup> select_PackageList = new List<SP_Select_PackageGroup>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "PackageGroup");
                var A = new SqlParameter("@A", "PackageGroupList");
                var B = new SqlParameter("@B", PackageGroupId.ToString());
                select_PackageList = ctx.Database.SqlQuery<SP_Select_PackageGroup>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                // select_PackageList = select_PackageList.Where(c => c.PackageGroupName.Contains(filter.PackageType) || String.IsNullOrEmpty(filter.PackageType) || String.IsNullOrEmpty(filter.PackageGroupName) || c.PackageGroupName.Contains(filter.PackageGroupName) ).ToList();
            }
            return select_PackageList;
        }
        public static List<SP_Select_PackageGroupConfiguration> GetPackageGroupConfigurations(long Id,long PackageGroupId)
        {
            List<SP_Select_PackageGroupConfiguration> select_PackageGroupConfigurations = new List<SP_Select_PackageGroupConfiguration>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "PackageGroup");
                var A = new SqlParameter("@A", "GroupConfig");
                var B = new SqlParameter("@B", Id.ToString());
                var C = new SqlParameter("@C", PackageGroupId.ToString());
                select_PackageGroupConfigurations = ctx.Database.SqlQuery<SP_Select_PackageGroupConfiguration>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C", Module, A, B,C).ToList();
                // select_PackageList = select_PackageList.Where(c => c.PackageGroupName.Contains(filter.PackageType) || String.IsNullOrEmpty(filter.PackageType) || String.IsNullOrEmpty(filter.PackageGroupName) || c.PackageGroupName.Contains(filter.PackageGroupName) ).ToList();
            }
            return select_PackageGroupConfigurations;
        }

        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_PackageGroup mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var pacMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "PackageGroup", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    PackageGroup_Master _Master = new PackageGroup_Master();
                    _Master = (PackageGroup_Master)pacMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.PackageGroup_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_PackageGroup  mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var pac = db.PackageGroup_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    pac.PackageGroupName = mdl.PackageGroupName;
                    pac.PackageTypeId = mdl.PackageTypeId;
                    pac.IsActive = mdl.IsActive;
                    string DateAndTime = Common.GetCurrentDateTime();
                    pac.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    pac.UpdatedUserId = UserSession.UserId;
                    db.Entry(pac).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = pac.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = pac.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = pac.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.PackageGroup_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.IsSuccess = true;
                        response.Message = "Deleted Successfully";

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region Validation
        public static bool IsPackageGroupAleradyExists(string PackageGroupName, long id = 0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.PackageGroup_Master.Any(s => s.PackageGroupName.ToLower() == PackageGroupName.ToLower() && s.Id != id);
                }
                else
                {
                    IsSuccess = db.PackageGroup_Master.Any(s => s.PackageGroupName.ToLower() == PackageGroupName.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }
        public static bool IsPackageGroupExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.PackageGroup_Master.Any(e => e.Id == id);
        }
        #endregion
    }
}
