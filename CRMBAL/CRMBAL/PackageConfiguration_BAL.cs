﻿using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMBAL
{
    public class PackageConfiguration_BAL
    {
        #region ---Select Method---
        public static List<SP_Select_PackageConfiguration> GetPackageRestrictionByUserSubscription(long UserId, int PackageId,long SubscriptionId=0)
        {
            List<SP_Select_PackageConfiguration> select_PackageConfig = new List<SP_Select_PackageConfiguration>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var User = new SqlParameter("@ModuleName", "User");
                var UserList = new SqlParameter("@A", "UserSubscriptionPackage");
                var B = new SqlParameter("@B", UserId.ToString());
                var C = new SqlParameter("@C", PackageId.ToString());
                var D = new SqlParameter("@D", SubscriptionId.ToString());
                // List<SP_Select_User> select_Users = new List<SP_Select_User>();

                select_PackageConfig = ctx.Database.SqlQuery<SP_Select_PackageConfiguration>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C,@D", User, UserList, B, C,D).ToList();

            }

            return select_PackageConfig;

        }

        public static List<SP_Select_UserPackageSubscription> GetUserPackageRestrictionByUser(long UserId, int PackageId, long SubscriptionId = 0)
        {
            List<SP_Select_UserPackageSubscription> select_PackageConfig = new List<SP_Select_UserPackageSubscription>();

            using (var ctx = new db_SalesCRMEntities())
            {
                var User = new SqlParameter("@ModuleName", "User");
                var UserList = new SqlParameter("@A", "UserSubscriptionVerification");
                var B = new SqlParameter("@B", UserId.ToString());
                var C = new SqlParameter("@C", PackageId.ToString());
                var D = new SqlParameter("@D", SubscriptionId.ToString());
                // List<SP_Select_User> select_Users = new List<SP_Select_User>();

                select_PackageConfig = ctx.Database.SqlQuery<SP_Select_UserPackageSubscription>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C,@D", User, UserList, B, C, D).ToList();

            }

            return select_PackageConfig;

        }
        #endregion
        public static Task<Response_mdl> Add(SP_Select_PackageConfiguration mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var pacMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "PackageConfiguration", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    PackageConfiguration_Master _Master = new PackageConfiguration_Master();
                    _Master = (PackageConfiguration_Master)pacMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.PackageConfiguration_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }

        public static Task<Response_mdl> Update(SP_Select_PackageConfiguration mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var pac = db.PackageConfiguration_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    pac.RestrictionKey = mdl.RestrictionKey;
                    pac.RestrictionDependency = mdl.RestrictionDependency;
                    pac.RestrictionValue = mdl.RestrictionValue;

                    string DateAndTime = Common.GetCurrentDateTime();
                    pac.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    pac.UpdatedUserId = UserSession.UserId;
                    db.Entry(pac).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = pac.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = pac.Id;

                        response.Message = "Rows not Updated.";
                        mdl.Id = pac.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }


        public static bool IsPackageAleradyExists(string RestrictionKey, long id = 0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.PackageConfiguration_Master.Any(s => s.RestrictionKey.ToLower() == RestrictionKey.ToLower() && s.Id != id);
                }
                else
                {
                    IsSuccess = db.PackageConfiguration_Master.Any(s => s.RestrictionKey.ToLower() == RestrictionKey.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }

        public static List<SP_Select_PackageConfiguration> GetPackageConfiguration(long Id = 0)
        {
            List<SP_Select_PackageConfiguration> select_PackageConfiguration = new List<SP_Select_PackageConfiguration>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "PackageConfiguration");
                var A = new SqlParameter("@A", "GetPackageConfiguration");
                var B = new SqlParameter("@B", (Id > 0 ? Id.ToString() : "0"));
                //var C = new SqlParameter("@C", (ContactId > 0 ? ContactId.ToString() : "0"));
                select_PackageConfiguration = ctx.Database.SqlQuery<SP_Select_PackageConfiguration>("exec SP_SELECT_DATA @ModuleName,@A,@B,@C", Module, A, B, 0).ToList();

            }
            return select_PackageConfiguration;

        }
    }
}
