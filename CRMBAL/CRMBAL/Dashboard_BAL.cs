﻿using CRMEntity;
using CRMModel.CustomEntity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMUtility;
namespace CRMBAL
{
    public class Dashboard_BAL
    {
        public static List<SP_Select_Dashboard> GetDashboardCount()
        {
            List<SP_Select_Dashboard> select_Contacts = new List<SP_Select_Dashboard>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Dashboard");
                var A = new SqlParameter("@A", "All");
                string Date = Common.GetCurrentDateTime("Date");
                var B = new SqlParameter("@B",Date);
                select_Contacts = ctx.Database.SqlQuery<SP_Select_Dashboard>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();

            }
            return select_Contacts;

        }

    }
}
