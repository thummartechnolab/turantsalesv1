﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;
namespace CRMBAL
{
    public class Designation_BAL
    {
        #region ---Grid Selection---
        public static Task<SP_Select_Designation> GetFilter(NameValueCollection filter)
        {
            SP_Select_Designation desig = new SP_Select_Designation()
            {

                Designation = filter["Designation"]
            };
            return Task.FromResult<SP_Select_Designation>(desig);
        }
        public static List<SP_Select_Designation> GetDesignationJsGridList(NameValueCollection objfilter, long UserId, string UserTypeCode = "AA")
        {
            List<SP_Select_Designation> select_Designations = new List<SP_Select_Designation>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Designation");
                var A = new SqlParameter("@A", "DesignationList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_Designations = ctx.Database.SqlQuery<SP_Select_Designation>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                select_Designations = select_Designations.Where(c => c.UserId == UserId && (String.IsNullOrEmpty(filter.Designation) || c.Designation.ToLower().Contains(filter.Designation.ToLower()))).ToList();
            }

            return select_Designations;

        }
        #endregion
        #region ---Select Method---
        public static List<SP_AutoComplete> GetDesignationAutoComplete(long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            autoCompletes = Common_BAL.GetAutoComplete("Id", "Designation", "Designation_Master", UserId, SearchText);
            return autoCompletes;
        }

        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_Designation mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var indMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "Designation", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    Designation_Master _Master = new Designation_Master();
                    _Master = (Designation_Master)indMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.UserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.Designation_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Designation mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var desg = db.Designation_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    desg.Designation = mdl.Designation;
                    desg.IsActive = mdl.IsActive;
                    string DateAndTime = Common.GetCurrentDateTime();
                    desg.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    desg.UpdatedUserId = UserSession.UserId;
                    db.Entry(desg).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = desg.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = desg.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = desg.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var designation = db.Designation_Master.Where(s => s.Id == id).FirstOrDefault();
                    designation.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    designation.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    designation.UpdatedUserId = UserSession.UserId;
                    db.Entry(designation).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = designation.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion

        #region ---Verification---
        public static Designation_Master GetDesignationDetail(string Designation)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            return db.Designation_Master.Where(s => s.Designation.ToLower() == Designation.ToLower()).FirstOrDefault();
        }
        public static bool IsDesignationAleradyExists(string Designation, long id = 0,long UserId=0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.Designation_Master.Any(s => s.Designation.ToLower() == Designation.ToLower() && s.Id != id);
                }
               else if (UserId > 0)
                {
                    IsSuccess = db.Designation_Master.Any(s => s.Designation.ToLower() == Designation.ToLower() && s.UserId == UserId);
                }
                else
                {
                    IsSuccess = db.Designation_Master.Any(s => s.Designation.ToLower() == Designation.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }
        public static bool IsdesignationExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.Designation_Master.Any(e => e.Id == id);
        }
        #endregion

        public static Designation_Master Fetch_Designation_Master_by_ID(int id)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            Designation_Master DesignationMaster = new Designation_Master();
            DesignationMaster = db.Designation_Master.Where(s => s.Id == id).FirstOrDefault();
            return DesignationMaster;
        }
    }
}
