﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMEntity;
using CRMModel;
using CRMModel.CustomEntity;
using CRMModel.ViewModel;
using CRMUtility;

namespace CRMBAL
{
    public class Industry_BAL
    {
        #region ---Grid Selection---
        public static Task<SP_Select_Industry> GetFilter(NameValueCollection filter)
        {
            SP_Select_Industry industry = new SP_Select_Industry()
            {

                IndustryType = filter["IndustryType"]
            };
            return Task.FromResult<SP_Select_Industry>(industry);
        }
        public static List<SP_Select_Industry> GetIndustryJsGridList(NameValueCollection objfilter,long UserId, string UserTypeCode = "AA")
        {
            List<SP_Select_Industry> select_Industries = new List<SP_Select_Industry>();
            var filter = GetFilter(objfilter).Result;
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Industry");
                var A = new SqlParameter("@A", "IndustryList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_Industries = ctx.Database.SqlQuery<SP_Select_Industry>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
                select_Industries = select_Industries.Where(c => c.UserId == UserId && (String.IsNullOrEmpty(filter.IndustryType) || c.IndustryType.ToLower().Contains(filter.IndustryType.ToLower()))).ToList();
            }

            return select_Industries;

        }
        public static List<SP_AutoComplete> GetIndustryAutoComplete(long UserId, string SearchText)
        {
            List<SP_AutoComplete> autoCompletes = new List<SP_AutoComplete>();
            autoCompletes = Common_BAL.GetAutoComplete("Id", "IndustryType", "IndustryType_Master", UserId, SearchText);
            return autoCompletes;
        }
        #endregion
        #region ---Selection Method---
        public static List<SP_Select_Industry> GetIndustries(long UserId)
        {
            List<SP_Select_Industry> select_Industries = new List<SP_Select_Industry>();
            using (var ctx = new db_SalesCRMEntities())
            {
                var Module = new SqlParameter("@ModuleName", "Industry");
                var A = new SqlParameter("@A", "IndustryList");
                var B = new SqlParameter("@B", UserId.ToString());
                select_Industries = ctx.Database.SqlQuery<SP_Select_Industry>("exec SP_SELECT_DATA @ModuleName,@A,@B", Module, A, B).ToList();
            }

            return select_Industries;
        }
        #endregion
        #region --Insert Update Operation--
        public static Task<Response_mdl> Add(SP_Select_Industry mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {
                using (var db = new db_SalesCRMEntities())
                {
                    var indMst = CustomLinq_BAL.GetViewModel(En_EntityRequestType.EntityModel.ToString(), "Industry", En_EntityType.FirtOrDefault.ToString(), null, mdl);
                    IndustryType_Master _Master = new IndustryType_Master();
                    _Master = (IndustryType_Master)indMst;
                    string DateAndTime = Common.GetCurrentDateTime();
                    _Master.CreadtedUserId = UserSession.UserId;
                    _Master.UserId = UserSession.UserId;
                    _Master.CreatedDateTime = Convert.ToDateTime(DateAndTime);
                    db.IndustryType_Master.Add(_Master);
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;
                        response.Id = _Master.Id;
                        response.Message = "Added Successfully";
                        mdl.Id = _Master.Id;
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = _Master.Id;
                        response.Message = "Rows not affected.";
                        mdl.CreatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        mdl.UpdatedOn = "";
                        response.Data = mdl;
                    }
                }

            }
            catch (Exception ex)
            {

                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.IsErrorOccur = true;
            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Update(SP_Select_Industry mdl)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var ind = db.IndustryType_Master.Where(s => s.Id == mdl.Id).FirstOrDefault();
                    ind.IndustryType = mdl.IndustryType;
                    ind.IsActive = mdl.IsActive;
                    string DateAndTime = Common.GetCurrentDateTime();
                    ind.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    ind.UpdatedUserId = UserSession.UserId;
                    db.Entry(ind).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        response.Id = ind.Id;
                        response.IsSuccess = true;

                        response.Message = "Updated Successfully";
                        mdl.UpdatedOn = Convert.ToDateTime(DateAndTime).ToString("dd-MM-yyyy hh:mm tt");
                        response.Data = mdl;

                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = ind.Id;
                        response.Message = "Rows not Updated.";
                        mdl.Id = ind.Id;
                        response.Data = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        public static Task<Response_mdl> Delete(int id)
        {
            Response_mdl response = new Response_mdl();
            try
            {

                using (var db = new db_SalesCRMEntities())
                {
                    var user = db.IndustryType_Master.Where(s => s.Id == id).FirstOrDefault();
                    user.IsDeleted = true;
                    string DateAndTime = Common.GetCurrentDateTime();
                    user.UpdatedDateTime = Convert.ToDateTime(DateAndTime);
                    user.UpdatedUserId = UserSession.UserId;
                    db.Entry(user).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {

                        response.IsSuccess = true;

                        response.Message = "Deleted Successfully";


                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Id = user.Id;
                        response.Message = "Rows not Deleted.";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Id = 0;
                response.IsSuccess = false;
                response.Message = ex.Message;

            }
            return Task.FromResult<Response_mdl>(response);
        }
        #endregion
        #region ---Verification---
        public static IndustryType_Master GetIndustryDetail(string IndustryType)
        {
            db_SalesCRMEntities db = new db_SalesCRMEntities();
            return db.IndustryType_Master.Where(s => s.IndustryType.ToLower() == IndustryType.ToLower()).FirstOrDefault();
        }
        public static bool IsIndustryAleradyExists(string IndustryType, long id = 0,long UserId=0)
        {
            bool IsSuccess = false;
            try
            {
                db_SalesCRMEntities db = new db_SalesCRMEntities();
                if (id > 0)
                {
                    IsSuccess = db.IndustryType_Master.Any(s => s.IndustryType.ToLower() == IndustryType.ToLower() && s.Id != id);
                }
                else if (UserId > 0)
                {
                    IsSuccess = db.IndustryType_Master.Any(s => s.IndustryType.ToLower() == IndustryType.ToLower() && s.UserId == UserId);
                }
                else
                {
                    IsSuccess = db.IndustryType_Master.Any(s => s.IndustryType.ToLower() == IndustryType.ToLower());
                }

            }
            catch (Exception ex)
            {


            }
            return IsSuccess;
        }
        public static bool IsIndustryExists(long id)
        {
            db_SalesCRMEntities ctx = new db_SalesCRMEntities();
            return ctx.IndustryType_Master.Any(e => e.Id == id);
        }
        #endregion

    }
}
