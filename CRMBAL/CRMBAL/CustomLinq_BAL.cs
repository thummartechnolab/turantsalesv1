﻿using CRMModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMModel.ViewModel;
using CRMEntity;
using CRMModel.CustomEntity;

namespace CRMBAL
{
    public class CustomLinq_BAL
    {
        public static object GetViewModel(string RequestType, string ModelType, string EntityType, object EntityModel, object ViewModel)
        {
            object output = new object();

            if (RequestType == En_EntityRequestType.ViewModel.ToString())
            {
                if (ModelType == "User")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {


                        //output = mdl;
                    }
                }
            }
            else if (RequestType == En_EntityRequestType.EntityModel.ToString())
            {
                if (ModelType == "User")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var user = (User_mdl)ViewModel;

                        User_Master mdl = new User_Master();
                        mdl.FirstName = user.FirstName;
                        mdl.MiddleName = user.MiddleName;
                        mdl.LastName = user.LastName;
                        mdl.WorkEmail = user.Email;
                        mdl.ContactNo = user.ContactNo;
                        mdl.UserUniqueId = user.UserUniqueId;
                        mdl.UserTypeId = user.UserTypeId;
                        if (!String.IsNullOrEmpty(user.Gender))
                        {
                            mdl.Gender = (user.Gender == En_Gender.Male.ToString() ? "M" : "F");
                        }
                        mdl.IsActive = true;
                        mdl.IsDeleted = false;
                        mdl.IsApproved = false;
                        mdl.CompanyId = 0;
                        output = mdl;
                    }
                }
                else if (ModelType == "Industry")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var ind = (SP_Select_Industry)ViewModel;
                        IndustryType_Master mdl = new IndustryType_Master();
                        mdl.Id = ind.Id;
                        mdl.IndustryType = ind.IndustryType;
                        mdl.IsActive = ind.IsActive;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
                else if (ModelType == "Product")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var prd = (SP_Select_Product)ViewModel;
                        Product_Master mdl = new Product_Master();
                        mdl.Id = prd.Id;
                        mdl.ProductName = prd.ProductName;
                        mdl.IsActive = prd.IsActive;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
                else if (ModelType == "Organization")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var org = (SP_Select_Organization)ViewModel;
                        Organisation_Master mdl = new Organisation_Master();
                        mdl.Id = org.Id;
                        mdl.IndustryTypeId = org.IndustryType;
                        mdl.OrganisationName = org.OrganisationName;
                        mdl.IsActive = org.IsActive;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
                else if (ModelType == "Designation")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var desg = (SP_Select_Designation)ViewModel;
                        Designation_Master mdl = new Designation_Master();
                        mdl.Id = desg.Id;
                        mdl.Designation = desg.Designation;
                        mdl.IsActive = desg.IsActive;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
                else if (ModelType == "City")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var city = (SP_Select_City)ViewModel;
                        City_Master mdl = new City_Master();
                        mdl.Id = city.Id;
                        mdl.CityName = city.CityName;
                        mdl.IsActive = city.IsActive;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
                else if (ModelType == "Announcement")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var annous = (SP_Select_Announcement)ViewModel;
                        Announcement_Master mdl = new Announcement_Master();
                        mdl.id = annous.Id;
                        mdl.AnnouceMent = annous.AnnouceMent;
                        mdl.IsActive = annous.IsActive;
                        mdl.IsDeleted = false;
                        mdl.IsMember = annous.IsMember;
                        output = mdl;
                    }
                }
                else if (ModelType == "Package")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var Pac = (SP_Select_Package)ViewModel;
                        Package_Master mdl = new Package_Master();
                        mdl.Id = Pac.Id;
                        mdl.PackageName = Pac.PackageName;
                        mdl.ValidityInDays = Pac.ValidityInDays;
                        mdl.ValidityMonths = Pac.ValidityMonths;
                        mdl.PackageGroupId = Pac.PackageGroupId;
                        mdl.Price = Pac.Price;
                        mdl.TaxId = Pac.TaxId;
                        mdl.CurrencyCode = "INR";
                        mdl.IsAnyRestriction = Pac.IsAnyRestriction;
                        mdl.IsTaxablePrice = Pac.IsTaxablePrice;
                        mdl.IsActive = Pac.IsActive;
                        mdl.IsDisplay = Pac.IsDisplay;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
                else if (ModelType == "PackageGroup")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var Pac = (SP_Select_PackageGroup)ViewModel;
                        PackageGroup_Master mdl = new PackageGroup_Master();
                        mdl.Id = Pac.Id;
                        mdl.PackageGroupName = Pac.PackageGroupName;
                        mdl.PackageTypeId = Pac.PackageTypeId;
                        mdl.IsActive = Pac.IsActive;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
                else if (ModelType == "PackageConfiguration")
                {
                    if (EntityType == En_EntityType.FirtOrDefault.ToString())
                    {
                        var Pac = (SP_Select_PackageConfiguration)ViewModel;
                        PackageConfiguration_Master mdl = new PackageConfiguration_Master();
                        mdl.Id = Pac.Id;
                        mdl.RestrictionKey = Pac.RestrictionKey;
                        mdl.RestrictionValue = Pac.RestrictionValue;
                        mdl.RestrictionDependency = Pac.RestrictionDependency;
                        mdl.IsActive = Pac.IsActive;
                        mdl.IsDeleted = false;
                        output = mdl;
                    }
                }
            }
            return output;
        }
    }
}
