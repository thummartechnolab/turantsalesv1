﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace CRMUtility
{
    public static class CookieHandler
    {
        public static bool SetCookie(ref HttpResponseBase Response, UserCookie user, int ExpireMonths = 1)
        {
            bool IsSuccess = false;
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                string jsonData = js.Serialize(user);
                var UserEncrypt = Confidential.Encrypt(jsonData);
                HttpCookie hc = new HttpCookie("User", UserEncrypt);
                hc.Expires = DateTime.Now.AddMonths(ExpireMonths);
                Response.Cookies.Add(hc);
                IsSuccess = true;
            }
            catch
            {

            }
            return IsSuccess;
        }
        public static bool GetCookie(ref HttpRequestBase Request)
        {
            bool IsSuccess = false;
            string Cookie = "";
            if (Request.Cookies["User"] != null)
            {
                var UserEncrypt = Request.Cookies["User"].Value.ToString();
                try
                {
                    Cookie = Confidential.Decrypt(UserEncrypt);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    UserCookie objQ = new UserCookie();
                    objQ = js.Deserialize<UserCookie>(Cookie);
                    IsSuccess = ResetUserSession.ResetSession(objQ);


                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                IsSuccess = false;
            }
            return IsSuccess;
        }
        public static bool DeleteCookie(ref HttpResponseBase Response, int ExpireMonths = 1)
        {
            bool IsSuccess = false;
            try
            {
                HttpCookie hc = new HttpCookie("User");
                hc.Expires = DateTime.Now.AddMonths(-1);
                Response.Cookies.Add(hc);
                IsSuccess = true;
            }
            catch
            {

            }
            return IsSuccess;
        }
    }
}
