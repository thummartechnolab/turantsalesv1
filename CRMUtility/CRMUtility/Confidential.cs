﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
namespace CRMUtility
{
    public class Confidential
    {
        public static string GenerateHash(string SourceText)
        {
            try
            {
                //Create an encoding object to ensure the encoding standard for the source text
                UnicodeEncoding Ue = new UnicodeEncoding();
                //Retrieve a byte array based on the source text
                byte[] ByteSourceText = Ue.GetBytes(SourceText.ToString());
                //Instantiate an MD5 Provider object
                MD5CryptoServiceProvider Md5 = new MD5CryptoServiceProvider();
                //Compute the hash value from the source
                byte[] ByteHash = Md5.ComputeHash(ByteSourceText);
                //And convert it to String format for return
                return Convert.ToBase64String(ByteHash);
            }
            catch (Exception ex)
            {
                return "";
            }


        }

        public static string Encrypt(string encryptString)
        {
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //string EncryptionKey = "95865953752873SKYZSKYZSKYZSKYZSKYZSK";
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //string EncryptionKey = "9586595375SkYz2873";
            //string EncryptionKey = "95865953752873SKYZSKYZSKYZSKYZSKYZSK";
            cipherText = cipherText.Replace(" ", "+");
            //cipherText = cipherText.Replace(@"\", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
}
