﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CRMUtility
{
    public class UserSession
    {
        public static long UserId
        {
            get
            {
                return HttpContext.Current.Session["UserId"]!=null?Convert.ToInt32(HttpContext.Current.Session["UserId"].ToString()):0;
            }

            set
            {
                System.Web.HttpContext.Current.Session["UserId"] = value;
            }

        }
        public static string UserName
        {
            get
            {
                return HttpContext.Current.Session["UserName"] != null ? HttpContext.Current.Session["UserName"].ToString() : "";
            }

            set
            {
                System.Web.HttpContext.Current.Session["UserName"] = value;
            }

        }

        public static int UserTypeId
        {
            get
            {
                return HttpContext.Current.Session["UserTypeId"]!=null? Convert.ToInt32(HttpContext.Current.Session["UserTypeId"].ToString()):0;
            }

            set
            {
                System.Web.HttpContext.Current.Session["UserTypeId"] = value;
            }

        }
        public static string UserTypeCode
        {
            get
            {
                return HttpContext.Current.Session["UserTypeCode"] != null ? HttpContext.Current.Session["UserTypeCode"].ToString() : "";
            }

            set
            {
                System.Web.HttpContext.Current.Session["UserTypeCode"] = value;
            }

        }
    }
    public class UserCookie
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int UserTypeId { get; set; }
        public string UserTypeCode { get; set; }
    }
    public class ResetUserSession
    {
        public static bool ResetSession(UserCookie userCookie)
        {
            bool IsSuccess = false;
            try
            {
                UserSession.UserId = userCookie.UserId;
                UserSession.UserName = userCookie.UserName;
                UserSession.UserTypeId = userCookie.UserTypeId;
                UserSession.UserTypeCode = userCookie.UserTypeCode;
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;

            }
            return IsSuccess;
        }

    }
}
