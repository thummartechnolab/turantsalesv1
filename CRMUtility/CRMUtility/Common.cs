﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMUtility
{
   public static class Common
    {
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        public static string GetCurrentDateTime(string Type = "DateAndTime", string DateFormate = "yyyy-MM-dd", string TimeFormate = "HH:mm:ss")
        {
            string Result = "";
            DateTime IndianDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            //string Date = IndianDateTime.ToString("yyyy-MM-dd");
            //string Time = IndianDateTime.ToString("HH:mm:ss");
            string Date = IndianDateTime.ToString(DateFormate);
            string Time = IndianDateTime.ToString(TimeFormate);
            string DateAndTime = Date + " " + Time;
            if (Type == "DateAndTime")
            {
                Result = DateAndTime;
            }
            else if (Type == "Date")
            {
                Result = Date;
            }
            else if (Type == "Time")
            {
                Result = Time;
            }
            else
            {
                Result = DateAndTime;
            }

            return Result;
        }
        public static string RemoveSpecialCherector(string Source, char DefaultChar)
        {
            string FinalString = "";
            FinalString = Source;
            if (Source.Contains("/"))
            {
                FinalString = FinalString.Replace('/', DefaultChar);
            }
            if (Source.Contains("-"))
            {
                FinalString = FinalString.Replace('-', DefaultChar);
            }
            if (Source.Contains(" "))
            {
                FinalString = FinalString.Replace(' ', DefaultChar);
            }
            if (Source.Contains(":"))
            {
                FinalString = FinalString.Replace(':', DefaultChar);
            }
            if (Source.Contains("."))
            {
                FinalString = FinalString.Replace('.', DefaultChar);
            }

            return FinalString;
        }
    }
}
