﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace CRMUtility
{
    public class Utility
    {
        public static string BccEmailId = "support@tech-ps.com";
        public static string GetSiteRoot()
        {
            String url = HttpContext.Current.Request.Url.ToString().Replace("//", "##");
            url = url.Substring(0, url.IndexOf(@"/") + 1);
            return url.Replace("##", "//");
        }
        public static string GetClientIPAddress()
        {
            if (System.Web.HttpContext.Current == null)
                return string.Empty;
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            // to featch single ip address ... below code change from nrupesh on 2-8-18 for solving AMT Cyber source issue
            string[] multipleip = ip.Split(',');
            if (multipleip.Length > 1)
            {
                ip = multipleip[multipleip.Length - 1];
            }

            return ip;
        }

        #region Mail Sending

        public static void SendMail(string msgBody, string toEmail, string subject)
        {
            try
            {
                BccEmailId = WebConfigurationManager.AppSettings["bcc"];
                MailMessage objEmail = new MailMessage();
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                objEmail.From = new MailAddress(WebConfigurationManager.AppSettings["from"]);
                objEmail.To.Add(new MailAddress(toEmail));
                objEmail.Subject = subject;
                objEmail.Body = msgBody;
                objEmail.IsBodyHtml = true;

                client.Host = WebConfigurationManager.AppSettings["host"];
                client.Port = Convert.ToInt32(WebConfigurationManager.AppSettings["port"]);
                client.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["username"], WebConfigurationManager.AppSettings["password"]);
                client.EnableSsl = Convert.ToBoolean(WebConfigurationManager.AppSettings["enablessl"]);

                if (!string.IsNullOrEmpty(BccEmailId))
                {
                    foreach (var bccMail in BccEmailId.Split(','))
                    {
                        objEmail.Bcc.Add(bccMail.Trim());
                    }
                }
                //client.UseDefaultCredentials = false;
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(objEmail);

            }
            catch (Exception ex)
            {
                new WS_Log().MyErrorLog("SendEmail", ex.Message.ToString());
            }
        }

        public static void SendMail(string msgBody, string[] Email, string subject)
        {
            try
            {
                MailMessage objEmail = new MailMessage();
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();

                objEmail.Body = msgBody;
                objEmail.IsBodyHtml = true;
                objEmail.Subject = subject;
                if (Email.Length > 0)
                {
                    objEmail.To.Add(new MailAddress(Email[0]));
                }
                for (int i = 1; i < Email.Length; i++)
                {
                    if (!string.IsNullOrEmpty(Email[i]))
                        objEmail.CC.Add(new MailAddress(Email[i]));
                }

                if (!string.IsNullOrEmpty(BccEmailId))
                {
                    foreach (var bccMail in BccEmailId.Split(','))
                    {
                        objEmail.Bcc.Add(bccMail.Trim());//To keep logs (Ticket #1837) Point no 3 and 4

                    }
                }

                //use for attach image in email template
                //objEmail = GetImageAlternetView(objEmail);
                client.Send(objEmail);

            }
            catch (Exception ex)
            {
                new WS_Log().MyErrorLog("SendEmail", ex.Message.ToString());

            }
        }
        #endregion
    }
}
