﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace CRMUtility
{
   public class WS_Log
    {
        public void MyErrorLog(string FileName, string Msg)
        {

            string dir = HostingEnvironment.MapPath("~/Mylog");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            try
            {
                string _name = Path.Combine(dir, FileName + "_" + DateTime.Now.ToString("dd-MM-yy-hhmmssffff") + "_" + new Random().Next(100, 999) + ".txt");
                File.WriteAllText(_name, Msg);
            }
            catch { }
        }
    }
}
